
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// BFPaperCheckbox
#define COCOAPODS_POD_AVAILABLE_BFPaperCheckbox
#define COCOAPODS_VERSION_MAJOR_BFPaperCheckbox 1
#define COCOAPODS_VERSION_MINOR_BFPaperCheckbox 1
#define COCOAPODS_VERSION_PATCH_BFPaperCheckbox 3

// DZNEmptyDataSet
#define COCOAPODS_POD_AVAILABLE_DZNEmptyDataSet
#define COCOAPODS_VERSION_MAJOR_DZNEmptyDataSet 1
#define COCOAPODS_VERSION_MINOR_DZNEmptyDataSet 4
#define COCOAPODS_VERSION_PATCH_DZNEmptyDataSet 1

// FMDB
#define COCOAPODS_POD_AVAILABLE_FMDB
#define COCOAPODS_VERSION_MAJOR_FMDB 2
#define COCOAPODS_VERSION_MINOR_FMDB 3
#define COCOAPODS_VERSION_PATCH_FMDB 0

// FMDB/common
#define COCOAPODS_POD_AVAILABLE_FMDB_common
#define COCOAPODS_VERSION_MAJOR_FMDB_common 2
#define COCOAPODS_VERSION_MINOR_FMDB_common 3
#define COCOAPODS_VERSION_PATCH_FMDB_common 0

// FMDB/standard
#define COCOAPODS_POD_AVAILABLE_FMDB_standard
#define COCOAPODS_VERSION_MAJOR_FMDB_standard 2
#define COCOAPODS_VERSION_MINOR_FMDB_standard 3
#define COCOAPODS_VERSION_PATCH_FMDB_standard 0

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 9
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 0

// UIColor+BFPaperColors
#define COCOAPODS_POD_AVAILABLE_UIColor_BFPaperColors
#define COCOAPODS_VERSION_MAJOR_UIColor_BFPaperColors 1
#define COCOAPODS_VERSION_MINOR_UIColor_BFPaperColors 2
#define COCOAPODS_VERSION_PATCH_UIColor_BFPaperColors 1

// iCarousel
#define COCOAPODS_POD_AVAILABLE_iCarousel
#define COCOAPODS_VERSION_MAJOR_iCarousel 1
#define COCOAPODS_VERSION_MINOR_iCarousel 8
#define COCOAPODS_VERSION_PATCH_iCarousel 0

