//
//  SecondViewController.h
//  VirtualWardrobe
//
//  Created by alex on 8/7/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKImagePickerViewController.h"
#import "MBProgressHUD.h"
#import "Data.h"
#import "PieceOfCloth.h"
#import "AlertComponent.h"
#import "KGDiscreetAlertView.h"

@interface SecondViewController : UITableViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate>

@end
