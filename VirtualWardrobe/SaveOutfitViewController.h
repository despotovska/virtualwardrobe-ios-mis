//
//  SaveOutfitViewController.h
//  VirtualWardrobe
//
//  Created by alex on 8/31/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data.h"
#import "MBProgressHUD.h"
#import "SevenSwitch.h"
#import "AlertComponent.h"
#import "KGDiscreetAlertView.h"

@interface SaveOutfitViewController : UITableViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate>

@property (strong, nonatomic) NSArray *clothes;

@end
