//
//  WellfleetFont.m
//  VirtualWardrobe
//
//  Created by alex on 9/3/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "WellfleetFont.h"

@implementation WellfleetFont

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"Wellfleet-Regular" size:self.font.pointSize];
}

@end
