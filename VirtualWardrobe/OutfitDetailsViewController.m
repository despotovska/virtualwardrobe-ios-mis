//
//  OutfitDetailsViewController.m
//  VirtualWardrobe
//
//  Created by alex on 9/5/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "OutfitDetailsViewController.h"

@interface OutfitDetailsViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *seasonLabel;
@property (strong, nonatomic) IBOutlet UILabel *occasionLabel;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descHeight;
@property (strong, nonatomic) IBOutlet UIButton *editButton;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;
@property (strong, nonatomic) AlertComponent *alertComponent;
@property (strong, nonatomic) UIView *modal;
@property (strong, nonatomic) UITextField *activeField;
@property (assign, nonatomic) BOOL keyboardShown;
@property (assign, nonatomic) BOOL viewMoved;

@end

@implementation OutfitDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"mala_hangers"]]];
    
    _deleteButton.layer.cornerRadius  = 10.0f;
    _deleteButton.layer.masksToBounds = YES;
    _editButton.layer.cornerRadius  = 10.0f;
    _editButton.layer.masksToBounds = YES;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.descHeight.constant = 50;
    _imageView.image = [UIImage imageWithContentsOfFile:_outfit.picture];
    _seasonLabel.text = _outfit.lastWeared;
    _occasionLabel.text = [self getOccasionForInt:[_outfit.occasion intValue]];
    _descriptionLabel.text = _outfit.description;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    										 selector:@selector(keyboardWasShown:)
    											 name:UIKeyboardDidShowNotification
    										   object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
    										 selector:@selector(keyboardWasHidden:)
    											 name:UIKeyboardDidHideNotification
    										   object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


- (NSString *)getOccasionForInt:(int)occasion
{
    NSString *result;
    switch (occasion) {
        case 1:
            result = @"Casual";
            break;
        case 2:
            result = @"Work";
            break;
        case 3:
            result = @"Work out";
            break;
        case 4:
            result = @"Weekend";
            break;
        case 5:
            result = @"Special occasion";
            break;
        default:
            break;
    }
    return result;
}

- (IBAction)deleteButton:(id)sender
{
    BOOL success = [Data deleteOutfit:_outfit];
    if(success)
        [self.navigationController popViewControllerAnimated:YES];
    else
    {
        self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Error"
                                                              andMessage:@"Something went wrong. Try again."
                                                         andButtonTitles:@[@"OK"]
                                                           andTargetView:self.view];
        [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
        }];
    }
}

- (IBAction)editButton:(id)sender
{
    UIImageView * bgimgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"han"]];
    _modal = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
    
    [_modal setBackgroundColor:[UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1]];
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    saveBtn.frame = CGRectMake(120, 0, 80, 40);
    [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    [saveBtn.titleLabel setFont:[UIFont fontWithName:@"Wellfleet-Regular" size:15.0]];
    [saveBtn setBackgroundColor:[UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1]];
    [saveBtn.titleLabel setTextColor:[UIColor whiteColor]];
    [saveBtn setTintColor:[UIColor whiteColor]];
    [saveBtn addTarget:self action:@selector(updateText:) forControlEvents:UIControlEventTouchUpInside];
    [_modal addSubview:saveBtn];
    
    UIView *textArea = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 320, 120)];
    [textArea setBackgroundColor:[UIColor whiteColor]];
    UITextView *descriptionText = [[UITextView alloc] initWithFrame:CGRectMake(10, 10, 300 , 100)];
    descriptionText.delegate = self;
    descriptionText.tag = 56;
    [descriptionText setFont:[UIFont fontWithName:@"Wellfleet-Regular" size:17.0]];
    descriptionText.text = _outfit.description;
    [textArea addSubview:descriptionText];
    [_modal addSubview:textArea];
    
    [self presentSemiView:_modal withOptions:@{ KNSemiModalOptionKeys.backgroundView:bgimgv, KNSemiModalOptionKeys.pushParentBack : @(YES),
                                               KNSemiModalOptionKeys.parentAlpha : @(0.9) }];
}

- (IBAction)updateText:(id)sender
{
    _outfit.description = ((UITextView *)[_modal viewWithTag:56]).text;
    BOOL success = [Data updateOutfit:_outfit];
    _descriptionLabel.text = _outfit.description;
    
    if (success && [self respondsToSelector:@selector(dismissSemiModalView)])
        [self dismissSemiModalView];
    else
    {
        self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Error"
                                                              andMessage:@"Something went wrong. Try again."
                                                         andButtonTitles:@[@"OK"]
                                                           andTargetView:self.view];
        [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
        }];
    }
}


#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark - Keyboard notifications

- (void)keyboardWasShown:(NSNotification *)aNotification
{
    if ( _keyboardShown )
    	return;
    
    NSDictionary *info = [aNotification userInfo];
    NSValue *aValue = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [aValue CGRectValue].size;
    NSTimeInterval animationDuration = 0.3;
    CGRect frame = _modal.frame;
    frame.origin.y -= keyboardSize.height;
    frame.size.height += keyboardSize.height;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.modal.frame = frame;
    [UIView commitAnimations];
    
    _viewMoved = YES;
    _keyboardShown = YES;
}

- (void)keyboardWasHidden:(NSNotification *)aNotification
{
    if ( _viewMoved ) {
    	NSDictionary *info = [aNotification userInfo];
    	NSValue *aValue = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    	CGSize keyboardSize = [aValue CGRectValue].size;
        
    	NSTimeInterval animationDuration = 0.3;
    	CGRect frame = _modal.frame;
    	frame.origin.y += keyboardSize.height;
    	frame.size.height -= keyboardSize.height;
    	[UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    	[UIView setAnimationDuration:animationDuration];
    	self.modal.frame = frame;
    	[UIView commitAnimations];
        
    	_viewMoved = NO;
    }
    
    _keyboardShown = NO;
}

@end
