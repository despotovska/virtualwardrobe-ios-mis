//
//  SaveOutfitViewController.m
//  VirtualWardrobe
//
//  Created by alex on 8/31/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "SaveOutfitViewController.h"

@interface SaveOutfitViewController ()

@property (strong, nonatomic) NSArray *seasons;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) IBOutlet UITextView *descriptionText;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIView *switchView;
@property (strong, nonatomic) AlertComponent *alertComponent;
@property (strong, nonatomic) NSString *placeholderText;

@end

@implementation SaveOutfitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _saveButton.enabled = YES;
    
    SevenSwitch *mySwitch2 = [[SevenSwitch alloc] initWithFrame:CGRectMake(120, 3, 80, 40)];
    [mySwitch2 addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    mySwitch2.offLabel.text = @"S";
    mySwitch2.onLabel.text = @"N";
    mySwitch2.thumbTintColor = [UIColor colorWithRed:(25.0/255) green:(104.0/255) blue:(80.0/255) alpha:1.00f];
    mySwitch2.onTintColor = [UIColor lightGrayColor];
    mySwitch2.isRounded = YES;
    [self.switchView addSubview:mySwitch2];

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];   
    BOOL north = [prefs boolForKey:@"north"];

    [mySwitch2 setOn:north animated:YES];
    
    _saveButton.layer.cornerRadius  = 12.0f;
    _saveButton.layer.masksToBounds = YES;
    [_saveButton.titleLabel setFont:[UIFont fontWithName:@"Wellfleet-Regular" size:_saveButton.titleLabel.font.pointSize]];
    
    _picker.delegate = self;
    _picker.dataSource = self;
    
    _placeholderText = @"insert description for searching outfits";
    
    _descriptionText.delegate = self;
    _descriptionText.text = _placeholderText;
    _descriptionText.textColor = [UIColor lightGrayColor];

    CGAffineTransform t0 = CGAffineTransformMakeTranslation (0,2*_picker.bounds.size.height/3);
    CGAffineTransform s0 = CGAffineTransformMakeScale (0.7, 0.7);
    CGAffineTransform t1 = CGAffineTransformMakeTranslation (0,-2*_picker.bounds.size.height/3);
    _picker.transform = CGAffineTransformConcat(t0, CGAffineTransformConcat(s0, t1));
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

    
    _seasons = [Data getSeasons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)addOutfit:(id)sender
{
    NSDate *myDate = [[NSDate date] dateByAddingTimeInterval:1];
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"yyyyMMddHHmmss"];
    NSString *picture = [dateFormater stringFromDate:myDate];
    picture = [picture stringByAppendingString:@".jpg"];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"VirtualWardrobe"];
    dataPath = [dataPath stringByAppendingPathComponent:picture];
    
    Outfit *o = [[Outfit alloc] init];
    o.season = [NSString stringWithFormat:@"%d", ([_picker selectedRowInComponent:0]+1)];
    
    if([_descriptionText.text isEqualToString:_placeholderText])
        o.description = @"";
    else
        o.description = _descriptionText.text;
    o.picture = dataPath;
    o.occasion = ((PieceOfCloth *)[_clothes objectAtIndex:0]).occasion;
    

    PieceOfCloth *pom = nil;
    PieceOfCloth *top = nil;
    NSInteger type = 0;
    
    for(NSInteger i = 0; i<[_clothes count]; i++)
    {
        pom = [_clothes objectAtIndex:i];
        if([pom.type intValue] == 1)
        {
            top = pom;
            break;
        }
    }
    
    if(top != nil)
    {
        if([_clothes count] == 3)
            type = 1;                  //top + bottom + shoes
        else if([_clothes count] == 4)
            type = 2;                   //top + bottom + shoes + 1
        else if([_clothes count] == 5)
            type = 3;                   //top + bottom + shoes + 2
        else if([_clothes count] == 6)
            type = 4;                   //top + bottom + shoes + 3
    }
    else
    {
        if([_clothes count] == 2)
            type = 5;                   //dress + shoes
        else if([_clothes count] == 3)
            type = 6;                   //dress + shoes + 1
        else if([_clothes count] == 4)
            type = 7;                   //dress + shoes + 2
        else if([_clothes count] == 5)
            type = 8;                   //dress + shoes + 3
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.detailsLabelText = [Data getRandomQuote];
    hud.square = YES;
    hud.minSize = CGSizeMake(140.f, 140.f);
    NSString *message = @"The outfit has been successfully added!";
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        int success = [Data addOutfit:o forPieces:_clothes];
        
        UIImage *outfitPic = [self imageOutfitForPieces:_clothes forType:type];
        if (outfitPic != nil && success == 1)
        {
            NSData * data = UIImageJPEGRepresentation(outfitPic,1);
            [data writeToFile:dataPath atomically:YES];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if(success == 1)
            {
                [self scrollToTop];
                KGDiscreetAlertView *alert = [[KGDiscreetAlertView alloc] init];
                alert.tapToHide = YES;
                [alert showWithText:message inViewWithNavigationBar:self.view maxWidth:320 delay:3];
                _saveButton.enabled = NO;
                _saveButton.alpha = 0.8;
            }
            else
            {
                NSString *msg;
                NSString *title;
                if(success == 0)
                {
                    msg = @"Something went wrong. Try again.";
                    title = @"Error";
                }
                else
                {
                    msg = @"You have already added \nthis outfit.";
                    title = @"Duplicate";
                    _saveButton.enabled = NO;
                    _saveButton.alpha = 0.8;
                }
                self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:title
                                                                      andMessage:msg
                                                                 andButtonTitles:@[@"OK"]
                                                                   andTargetView:self.view];
                [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
                }];
            }
            [self cancel];
            [self scrollToTop];
        });
    });
}


- (void) cancel
{
    _descriptionText.text = _placeholderText;
    _descriptionText.textColor = [UIColor lightGrayColor];
    [_picker selectRow:0 inComponent:0 animated:YES];
}


#pragma mark - UITableView DataSource Method

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 1)
		[self.descriptionText becomeFirstResponder];
}


#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:_placeholderText]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = _placeholderText;
        textView.textColor = [UIColor lightGrayColor];
    }
    [textView resignFirstResponder];
}

-(void)dismissKeyboard
{
    [_descriptionText resignFirstResponder];
}

#pragma mark - UIPickerView DataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return [_seasons count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [_seasons objectAtIndex:row];
}


#pragma mark - Private Methods for generating UIImage

- (UIImage *)imageOutfitForPieces:(NSArray *)pieces forType:(int)type
{
    UIImage *bg = [UIImage imageNamed:@"bg.png"];
    CGSize finalSize = [bg size];

    NSArray *imgPointArray = [self imgPointsArrayForType:type];
    
    UIGraphicsBeginImageContext(finalSize);
    [bg drawInRect:CGRectMake(0,0,finalSize.width,finalSize.height) blendMode:kCGBlendModeNormal alpha:0.2];
    
    int i = 0;
    for(PieceOfCloth *p in pieces)
    {
        UIImage *img;
        if([p.type intValue] != 3)
            img = [self scale:[UIImage imageWithContentsOfFile:p.picture] toSize:CGSizeMake(165, 135)];
        else
            img = [self scale:[UIImage imageWithContentsOfFile:p.picture] toSize:CGSizeMake(165, 280)];
        CGSize imgSize = [img size];
        [img drawInRect:CGRectMake([imgPointArray[i] floatValue],[imgPointArray[i+1] floatValue],imgSize.width,imgSize.height)];
        i+=2;
    }

    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (NSArray *)imgPointsArrayForType:(int)type
{
    NSArray *result = [NSArray array];
    
    switch (type) {
        case 1:
            result = [[NSArray alloc] initWithObjects:@"97.5", @"10",           // T
                                                      @"97.5", @"155",          // B
                                                      @"97.5", @"300", nil];    // S
            break;
            
        case 2:
            result = [[NSArray alloc] initWithObjects:@"10", @"82.5",           // T
                                                      @"10", @"227.5",          // B
                                                      @"185", @"82.5",          // .
                                                      @"185", @"227.5", nil];   // S
            break;
            
        case 3:
            result = [[NSArray alloc] initWithObjects:@"10", @"10",             // T
                                                      @"10", @"155",            // B
                                                      @"185", @"10",            // .
                                                      @"185", @"155",           // .
                                                      @"97.5", @"300", nil];    // S
            break;
            
        case 4:
            result = [[NSArray alloc] initWithObjects:@"10", @"10",             // T
                                                      @"10", @"155",            // B
                                                      @"185", @"10",            // . J
                                                      @"185", @"155",           // . JW
                                                      @"185", @"300",           // . A
                                                      @"10", @"300", nil];      // S
            break;
            
        case 5:
            result = [[NSArray alloc] initWithObjects:@"97.5", @"10",           // D
                                                      @"97.5", @"300", nil];    // S
            break;
            
        case 6:
            result = [[NSArray alloc] initWithObjects:@"10", @"82.5",           // D
                                                      @"185", @"82.5",          // .
                                                      @"185", @"227.5", nil];   // S
            break;
            
        case 7:
            result = [[NSArray alloc] initWithObjects:@"10", @"10",             // D
                                                      @"185", @"10",            // .
                                                      @"185", @"155",           // .
                                                      @"97.5", @"300", nil];    // S
            break;
            
        case 8:
            result = [[NSArray alloc] initWithObjects:@"10", @"10",             // D
                                                      @"185", @"10",            // . J
                                                      @"185", @"155",           // . JW
                                                      @"185", @"300",           // . A
                                                      @"10", @"300", nil];      // S
            break;
        default:
            result = nil;
            break;
    }
    
    return result;
}

- (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

#pragma mark - UISwitch State Changed Method

- (void)switchChanged:(SevenSwitch *)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:sender.on forKey:@"north"];
}


-(void) scrollToTop
{
    if ([self numberOfSectionsInTableView:self.tableView] > 0)
    {
        NSIndexPath* top = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
        [self.tableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

@end
