//
//  SecondViewController.m
//  VirtualWardrobe
//
//  Created by alex on 8/7/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController()

@property (strong, nonatomic) NSArray *occasions;
@property (strong, nonatomic) NSArray *types;
@property (strong, nonatomic) IBOutlet UIPickerView *picker;
@property (strong, nonatomic) IBOutlet UITextView *descriptionText;
@property (strong, nonatomic) IBOutlet UIButton *photoButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) PKImagePickerViewController *imagePicker;
@property (strong, nonatomic) AlertComponent *alertComponent;
@property (strong, nonatomic) NSString *placeholderText;

@end

@implementation SecondViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.topItem.title = @"Add a new piece of clothing";
    
    _photoButton.layer.cornerRadius  = 10.0f;
    _photoButton.layer.masksToBounds = YES;
    _saveButton.layer.cornerRadius  = 10.0f;
    _saveButton.layer.masksToBounds = YES;
    _cancelButton.layer.cornerRadius  = 10.0f;
    _cancelButton.layer.masksToBounds = YES;
    
    [_saveButton.titleLabel setFont:[UIFont fontWithName:@"Wellfleet-Regular" size:_saveButton.titleLabel.font.pointSize]];
    [_photoButton.titleLabel setFont:[UIFont fontWithName:@"Wellfleet-Regular" size:_photoButton.titleLabel.font.pointSize]];
    [_cancelButton.titleLabel setFont:[UIFont fontWithName:@"Wellfleet-Regular" size:_cancelButton.titleLabel.font.pointSize]];
    
    _picker.delegate = self;
    _picker.dataSource = self;
    
    _placeholderText = @"description is optional";
    
    _descriptionText.delegate = self;
    _descriptionText.text = _placeholderText;
    _descriptionText.textColor = [UIColor lightGrayColor];
    self.imagePicker = [[PKImagePickerViewController alloc] init];
    
    CGAffineTransform t0 = CGAffineTransformMakeTranslation (0,2*_picker.bounds.size.height/3);
    CGAffineTransform s0 = CGAffineTransformMakeScale (0.7, 0.7);
    CGAffineTransform t1 = CGAffineTransformMakeTranslation (0,-2*_picker.bounds.size.height/3);
    _picker.transform = CGAffineTransformConcat(t0, CGAffineTransformConcat(s0, t1));
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    _occasions = [Data getOccasions];
    _types = [Data getTypes];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - Photo Methods

- (IBAction)showCamera:(id)sender
{
    _imagePicker.dress = ([_picker selectedRowInComponent:1] == 2);
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (IBAction)cancelAdding:(id)sender
{
    [self cancel];
    [self scrollToTop];
}

- (void) cancel
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.selectedImage = nil;
    _descriptionText.text = _placeholderText;
    _descriptionText.textColor = [UIColor lightGrayColor];
    [_picker selectRow:0 inComponent:0 animated:YES];
    [_picker selectRow:0 inComponent:1 animated:YES];
}

- (BOOL)saveImage: (UIImage *)image withName: (NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"VirtualWardrobe"];
    dataPath = [dataPath stringByAppendingPathComponent:name];
    
    PieceOfCloth *p = [[PieceOfCloth alloc] init];
    p.occasion = [NSString stringWithFormat:@"%d", ([_picker selectedRowInComponent:0]+1)];
    p.type = [NSString stringWithFormat:@"%d", ([_picker selectedRowInComponent:1]+1)];
    if([_descriptionText.text isEqualToString:_placeholderText])
        p.description = @"";
    else
        p.description = _descriptionText.text;
    p.picture = dataPath;
    
    BOOL success = [Data addPieceOfCloth:p];
    
    if (image != nil && success)
    {
        NSData * data = UIImageJPEGRepresentation(image,1);
        [data writeToFile:dataPath atomically:YES];
    }
    
    return success;
}

- (IBAction)addNewPieceOfCloth:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(appDelegate.selectedImage != nil)
    {
        NSDate *myDate = [[NSDate date] dateByAddingTimeInterval:1];
        NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
        [dateFormater setDateFormat:@"yyyyMMddHHmmss"];
        NSString *picture = [dateFormater stringFromDate:myDate];
        picture = [picture stringByAppendingString:@".jpg"];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.detailsLabelText = [Data getRandomQuote];
        hud.square = YES;
        hud.minSize = CGSizeMake(140.f, 140.f);
        NSString *message = @"Item has been successfully added!";
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            BOOL success = [self saveImage:appDelegate.selectedImage withName:picture];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if(success)
                {
                    appDelegate.selectedImage = nil;
                    [self scrollToTop];
                    KGDiscreetAlertView *alert = [[KGDiscreetAlertView alloc] init];
                    alert.tapToHide = YES;
                    [alert showWithText:message inViewWithNavigationBar:self.view maxWidth:320 delay:3];
                    [self cancel];
                }
                else
                {
                    self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Error"
                                                                          andMessage:@"Something went wrong. Try again."
                                                                     andButtonTitles:@[@"OK"]
                                                                       andTargetView:self.view];
                    [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
                    }];
                }
            });
        });
    }
    else
    {
        self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Alert"
                                                              andMessage:@"You have to choose a photo first."
                                                         andButtonTitles:@[@"OK"]
                                                           andTargetView:self.view];
        [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
        }];
    }
}


#pragma mark - UITableView DataSource Method

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == 1)
		[self.descriptionText becomeFirstResponder];
}


#pragma mark - UITextView Delegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:_placeholderText]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = _placeholderText;
        textView.textColor = [UIColor lightGrayColor];
    }
    [textView resignFirstResponder];
}

- (void)dismissKeyboard
{
    [_descriptionText resignFirstResponder];
}


#pragma mark - UIPickerView DataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    if (component == 0) 
        return [_occasions count];
    return [_types count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    if (component == 0)
        return [_occasions objectAtIndex:row];
    return [_types objectAtIndex:row];
}


-(void) scrollToTop
{
    if ([self numberOfSectionsInTableView:self.tableView] > 0)
    {
        NSIndexPath* top = [NSIndexPath indexPathForRow:NSNotFound inSection:0];
        [self.tableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

@end
