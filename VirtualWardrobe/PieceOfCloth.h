//
//  PieceOfCloth.h
//  VirtualWardrobe
//
//  Created by alex on 8/24/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PieceOfCloth : NSObject

@property (nonatomic, strong) NSString *pid;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *occasion;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *picture;

@end
