//
//  TabBarController.m
//  VirtualWardrobe
//
//  Created by alex on 8/22/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "TabBarController.h"

@interface TabBarController ()

@property (strong, nonatomic) NSNumber *noPieces;
@property (strong, nonatomic) NSNumber *noOutfits;
@property (strong, nonatomic) NSNumber *time;
@property (strong, nonatomic) MenuComponent *menuComponent;
@property (assign, nonatomic) CGRect desiredMenuFrame;
-(void)showMenu:(UIGestureRecognizer *)gestureRecognizer;

@end

@implementation TabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UISwipeGestureRecognizer *showMenuGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showMenu:)];
    
    showMenuGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [self.view addGestureRecognizer:showMenuGesture];
        
    _desiredMenuFrame = CGRectMake(0.0, 0, 170, self.view.frame.size.height+20);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Private method - Menu

-(void)showMenu:(UIGestureRecognizer *)gestureRecognizer
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    _time = [prefs objectForKey:@"time"];
    _noOutfits= [prefs objectForKey:@"noOutfits"];
    _noPieces= [prefs objectForKey:@"noPieces"];
    
    NSInteger hours;
    NSInteger minutes;
    
    if(_time == nil)
    {
        _time = [NSNumber numberWithInt:0];
        [prefs setObject:_time forKey:@"time"];
        hours = 0;
        minutes = 0;
    }
    else
    {
        NSInteger time = [_time intValue];
        if(time < 60)
        {
            hours = 0;
            minutes = time;
        }
        else
        {
            hours = time/60;
            minutes = time%60;
        }
    }
    
    if(_noPieces == nil)
    {
        _noPieces = [NSNumber numberWithInt:0];
        [prefs setObject:_noPieces forKey:@"noPieces"];
    }
    
    if(_noOutfits == nil)
    {
        _noOutfits = [NSNumber numberWithInt:0];
        [prefs setObject:_noOutfits forKey:@"noOutfits"];
    }
    
    
    self.menuComponent = [[MenuComponent alloc] initMenuWithFrame:_desiredMenuFrame
                                                                                                   targetView:self.view
                                                                                                    direction:menuDirectionRightToLeft
                                                                                                      options:@[@"Statistics",@"Time",@"Clothes", @"Outfits",[NSString stringWithFormat:@"%dh %dm",hours, minutes],[_noPieces stringValue],[_noOutfits stringValue]]
                                                                                                 optionImages:@[@"bar",@"timeman", @"shirt_menu", @"outfit_menu"]];
    [self.menuComponent showMenuWithSelectionHandler:^(NSInteger selectedOptionIndex) {    }];
}

@end
