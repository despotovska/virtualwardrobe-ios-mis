//
//  ThirdViewController.h
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+EmptyDataSet.h"
#import "Data.h"
#import "Outfit.h"
#import "AlertComponent.h"
#import "KGDiscreetAlertView.h"
#import "SpringboardLayout.h"
#import "MBProgressHUD.h"
#import "OutfitCell.h"
#import "OutfitDetailsViewController.h"
#import "KGDiscreetAlertView.h"

@interface ThirdViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UISearchBarDelegate, SpringboardLayoutDelegate, UIGestureRecognizerDelegate>

@end
