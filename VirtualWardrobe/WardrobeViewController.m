//
//  WardrobeViewController.m
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "WardrobeViewController.h"

@interface WardrobeViewController ()

@property (strong, nonatomic) AlertComponent *alertComponent;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet iCarousel *modalCarousel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addOutfit;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewShoes;
@property (strong, nonatomic) IBOutlet UIButton *btnShoes;
@property (strong, nonatomic) IBOutlet UIImageView *imgJackets;
@property (strong, nonatomic) IBOutlet UIButton *btnJackets;
@property (strong, nonatomic) IBOutlet UIImageView *imgAccessories;
@property (strong, nonatomic) IBOutlet UIButton *btnAccessories;
@property (strong, nonatomic) IBOutlet UIImageView *imgJewleries;
@property (strong, nonatomic) IBOutlet UIButton *btnJewleries;
@property (strong, nonatomic)  UIButton *btnTops;
@property (strong, nonatomic)  UIButton *btnBottoms;
@property (strong, nonatomic)  UIButton *btnDresses;
@property (strong, nonatomic) IBOutlet UIView *jacketsContent;
@property (strong, nonatomic) IBOutlet UIView *accessoriesContent;
@property (strong, nonatomic) IBOutlet UIView *jewleryContent;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraint;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property BFPaperCheckbox *jacketsCheckbox;
@property BFPaperCheckbox *accessoriesCheckbox;
@property BFPaperCheckbox *jewleriesCheckbox;
@property (assign, nonatomic) int barButtonState;
@property (assign, nonatomic) int lastButton;
@property (strong, nonatomic) NSMutableArray *tops;
@property (strong, nonatomic) NSMutableArray *bottoms;
@property (strong, nonatomic) NSMutableArray *jackets;
@property (strong, nonatomic) NSMutableArray *ds;
@property (strong, nonatomic) NSMutableArray *shoes;
@property (strong, nonatomic) NSMutableArray *accessories;
@property (strong, nonatomic) NSMutableArray *jewleries;

@property (strong, nonatomic) NSArray *clothesForOutfitToSave;
@property (strong, nonatomic) NSMutableArray *currentFlags;

@end

@implementation WardrobeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"mala_hangers"]]];
    [_imgJackets.layer setBorderWidth:1];
    [_imgJackets.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [_imgJewleries.layer setBorderWidth:1];
    [_imgJewleries.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [_imgAccessories.layer setBorderWidth:1];
    [_imgAccessories.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [_imgViewShoes.layer setBorderWidth:1];
    [_imgViewShoes.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    [self.navigationController setTitle:@"Wardrobe"];
    
    self.constraint.constant = 450;
    _carousel.type = iCarouselTypeRotary;
    _carousel.pagingEnabled = YES;
    _carousel.clipsToBounds = YES;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [_jewleriesCheckbox uncheckAnimated:NO];
    [_jacketsCheckbox uncheckAnimated:NO];
    [_accessoriesCheckbox uncheckAnimated:NO];
    [_jacketsCheckbox setHidden:YES];
    [_accessoriesCheckbox setHidden:YES];
    [_jewleriesCheckbox setHidden:YES];
    _lastButton = 0;
    _barButtonState = Add;
    _addOutfit.image = [UIImage imageNamed:@"pluska"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.d getClothesForOccasion:_occasion];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            _tops = [appDelegate.d tops];
            _bottoms = [appDelegate.d bottoms];
            _jackets = [appDelegate.d jackets];
            _ds = [appDelegate.d ds];
            _shoes = [appDelegate.d shoes];
            _accessories = [appDelegate.d accessories];
            _jewleries = [appDelegate.d jewleries];
            [self setArrays];
            [self setButtonsAndImageViews];
            [_carousel reloadData];
            [self setCheckboxes];
        });
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    _carousel.delegate = nil;
    _carousel.dataSource = nil;
    _modalCarousel.delegate = nil;
    _modalCarousel.dataSource = nil;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - Private Methods

-(void)setArrays
{
    _clothesForOutfitToSave = [NSArray array];
    _currentFlags = [NSMutableArray array];
    
    if([_tops count] > 0)
        [_currentFlags addObject:[NSNumber numberWithInteger:0]];
    else
        [_currentFlags addObject:[NSNumber numberWithInteger:-1]];
    
    if([_bottoms count] > 0)
        [_currentFlags addObject:[NSNumber numberWithInteger:0]];
    else
        [_currentFlags addObject:[NSNumber numberWithInteger:-1]];
    
    if([_ds count] > 0)
        [_currentFlags addObject:[NSNumber numberWithInteger:0]];
    else
        [_currentFlags addObject:[NSNumber numberWithInteger:-1]];
    
    if([_shoes count] > 0)
        [_currentFlags addObject:[NSNumber numberWithInteger:0]];
    else
        [_currentFlags addObject:[NSNumber numberWithInteger:-1]];
    
    if([_jackets count] > 0)
        [_currentFlags addObject:[NSNumber numberWithInteger:0]];
    else
        [_currentFlags addObject:[NSNumber numberWithInteger:-1]];
    
    if([_accessories count] > 0)
        [_currentFlags addObject:[NSNumber numberWithInteger:0]];
    else
        [_currentFlags addObject:[NSNumber numberWithInteger:-1]];
    
    if([_jewleries count] > 0)
        [_currentFlags addObject:[NSNumber numberWithInteger:0]];
    else
        [_currentFlags addObject:[NSNumber numberWithInteger:-1]];
}

-(void)setButtonsAndImageViews
{
    _imgViewShoes.contentMode = UIViewContentModeScaleAspectFit;
    _imgViewShoes.clipsToBounds = YES;
    _imgViewShoes.image = [UIImage imageNamed:@"placeholder"];
    
    if(((NSNumber *)[_currentFlags objectAtIndex:Shoes]) == [NSNumber numberWithInteger:-1])
        _btnShoes.enabled = NO;
    else
    {
        _imgViewShoes.image = [UIImage imageWithContentsOfFile:((PieceOfCloth *)[_shoes objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Shoes]) intValue]]).picture];
        _btnShoes.enabled = YES;
    }
    
    
    _imgJackets.contentMode = UIViewContentModeScaleToFill;
    _imgJackets.clipsToBounds = YES;
    _imgJackets.image = [UIImage imageNamed:@"placeholder"];
    
    if(((NSNumber *)[_currentFlags objectAtIndex:Jackets]) == [NSNumber numberWithInteger:-1])
        _btnJackets.enabled = NO;
    else
    {
        _imgJackets.image = [UIImage imageWithContentsOfFile:((PieceOfCloth *)[_jackets objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Jackets]) intValue]]).picture];
        _btnJackets.enabled = YES;
    }
    
    
    _imgAccessories.contentMode = UIViewContentModeScaleToFill;
    _imgAccessories.clipsToBounds = YES;
    _imgAccessories.image = [UIImage imageNamed:@"placeholder"];

    if(((NSNumber *)[_currentFlags objectAtIndex:Accessories]) == [NSNumber numberWithInteger:-1])
        _btnAccessories.enabled = NO;
    else
    {
        _imgAccessories.image = [UIImage imageWithContentsOfFile:((PieceOfCloth *)[_accessories objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Accessories]) intValue]]).picture];
        _btnAccessories.enabled = YES;
    }
    
    
    _imgJewleries.contentMode = UIViewContentModeScaleToFill;
    _imgJewleries.clipsToBounds = YES;
    _imgJewleries.image = [UIImage imageNamed:@"placeholder"];
    
    if(((NSNumber *)[_currentFlags objectAtIndex:Jewlery]) == [NSNumber numberWithInteger:-1])
         _btnJewleries.enabled = NO;
    else
    {
        _imgJewleries.image = [UIImage imageWithContentsOfFile:((PieceOfCloth *)[_jewleries objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Jewlery]) intValue]]).picture];
        _btnJewleries.enabled = YES;
    }
}

-(void) addButonCheck
{
    if(((_carousel.currentItemIndex == 0) && (_btnTops.isEnabled == YES) && (_btnBottoms.isEnabled == YES))||((_carousel.currentItemIndex == 1) && (_btnDresses.isEnabled == YES)))
    {
        if(_btnShoes.isEnabled == YES)
            _addOutfit.enabled = YES;
        else _addOutfit.enabled = NO;
    }
    else
        _addOutfit.enabled = NO;
}

-(void)setCheckboxes
{
    _jacketsCheckbox = [[BFPaperCheckbox alloc] initWithFrame:CGRectMake(0, 15, 25, 25)];
    _jacketsCheckbox.center = CGPointMake(_jacketsCheckbox.center.x, _jacketsCheckbox.frame.origin.y);
    _jacketsCheckbox.tag = 1001;
    _jacketsCheckbox.rippleFromTapLocation = NO;
    _jacketsCheckbox.tapCirclePositiveColor = [UIColor paperColorAmber];
    _jacketsCheckbox.tapCircleNegativeColor = [UIColor paperColorRed];
    _jacketsCheckbox.checkmarkColor = [UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:0.8];
    if(((NSNumber *)[_currentFlags objectAtIndex:4]) != [NSNumber numberWithInteger:-1])
        [self.jacketsContent addSubview:_jacketsCheckbox];
    
    _accessoriesCheckbox = [[BFPaperCheckbox alloc] initWithFrame:CGRectMake(0, 15, 25, 25)];
    _accessoriesCheckbox.center = CGPointMake(_accessoriesCheckbox.center.x, _accessoriesCheckbox.frame.origin.y);
    _accessoriesCheckbox.tag = 1002;
    _accessoriesCheckbox.rippleFromTapLocation = NO;
    _accessoriesCheckbox.tapCirclePositiveColor = [UIColor paperColorAmber];
    _accessoriesCheckbox.tapCircleNegativeColor = [UIColor paperColorRed];
    _accessoriesCheckbox.checkmarkColor = [UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:0.8];
    if(((NSNumber *)[_currentFlags objectAtIndex:5]) != [NSNumber numberWithInteger:-1])
        [self.accessoriesContent addSubview:_accessoriesCheckbox];
    
    
    _jewleriesCheckbox = [[BFPaperCheckbox alloc] initWithFrame:CGRectMake(0, 15, 25, 25)];
    _jewleriesCheckbox.center = CGPointMake(_jewleriesCheckbox.center.x, _jewleriesCheckbox.frame.origin.y);
    _jewleriesCheckbox.tag = 1003;
    _jewleriesCheckbox.rippleFromTapLocation = NO;
    _jewleriesCheckbox.tapCirclePositiveColor = [UIColor paperColorAmber];
    _jewleriesCheckbox.tapCircleNegativeColor = [UIColor paperColorRed];
    _jewleriesCheckbox.checkmarkColor = [UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:0.8];
   if(((NSNumber *)[_currentFlags objectAtIndex:6]) != [NSNumber numberWithInteger:-1])
        [self.jewleryContent addSubview:_jewleriesCheckbox];
    
    [_jewleriesCheckbox uncheckAnimated:NO];
    [_jacketsCheckbox uncheckAnimated:NO];
    [_accessoriesCheckbox uncheckAnimated:NO];
    [_jewleriesCheckbox setHidden:YES];
    [_accessoriesCheckbox setHidden:YES];
    [_jacketsCheckbox setHidden:YES];
}


#pragma mark - iCarousel DataSource Methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    if(carousel == _carousel)
        return 2;
    else
        return [self currentModalDataSource].count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if(view == nil)
    {
        if (carousel == _carousel)
        {
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 180, 290)];
            [view setBackgroundColor:[UIColor whiteColor]];
            
            if(index == 0)
            {
                _btnTops = [UIButton buttonWithType:UIButtonTypeCustom];
                _btnTops.tag = 1;
                _btnTops.frame = CGRectMake(10, 6, 165, 135);
                [_btnTops addTarget:self action:@selector(buttonTap:)
                        forControlEvents:UIControlEventTouchUpInside];
                _btnTops.enabled = NO;
                
                _btnBottoms = [UIButton buttonWithType:UIButtonTypeCustom];
                _btnBottoms.tag = 2;
                _btnBottoms.frame = CGRectMake(10, 149, 165, 135);
                [_btnBottoms addTarget:self action:@selector(buttonTap:)
                  forControlEvents:UIControlEventTouchUpInside];
                _btnBottoms.enabled = NO;
                
                UIImageView *imgViewTop = [[UIImageView alloc] initWithFrame:CGRectMake(10, 6, 165, 135)];
                imgViewTop.tag = 101;
                imgViewTop.contentMode = UIViewContentModeScaleAspectFit;
                imgViewTop.clipsToBounds = YES;
                imgViewTop.image = [UIImage imageNamed:@"placeholder"];
                [imgViewTop.layer setBorderWidth:1];
                [imgViewTop.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
                
                UIImageView *imgViewBottom= [[UIImageView alloc] initWithFrame:CGRectMake(10, 149, 165, 135)];
                imgViewBottom.tag = 102;
                imgViewBottom.contentMode = UIViewContentModeScaleAspectFit;
                imgViewBottom.clipsToBounds = YES;
                imgViewBottom.image = [UIImage imageNamed:@"placeholder"];
                [imgViewBottom.layer setBorderWidth:1];
                [imgViewBottom.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];

                [view addSubview:imgViewTop];
                [view addSubview:_btnTops];
                [view addSubview:imgViewBottom];
                [view addSubview:_btnBottoms];
            }
            else
            {
                _btnDresses = [UIButton buttonWithType:UIButtonTypeCustom];
                _btnDresses.tag = 3;
                _btnDresses.frame = CGRectMake(10, 6, 165, 280);
                [_btnDresses addTarget:self action:@selector(buttonTap:)
                   forControlEvents:UIControlEventTouchUpInside];
                _btnDresses.enabled = NO;
                
                UIImageView *imgViewDress = [[UIImageView alloc] initWithFrame:CGRectMake(10, 6, 165, 280)];
                imgViewDress.tag = 103;
                imgViewDress.contentMode = UIViewContentModeScaleAspectFit;
                imgViewDress.clipsToBounds = YES;
                imgViewDress.image = [UIImage imageNamed:@"placeholder"];
                [imgViewDress.layer setBorderWidth:1];
                [imgViewDress.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
                
                [view addSubview:imgViewDress];
                [view addSubview:_btnDresses];
            }
        }
        else
        {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 250.0f, 200.0f)];
            view.contentMode = UIViewContentModeScaleAspectFit;
        }
    }
    
    if(carousel == _carousel)
    {
        if(index == 0)
        {
            _btnTops = (UIButton *)[view viewWithTag:1];
            _btnBottoms = (UIButton *)[view viewWithTag:2];
            
            UIImageView *imgViewTop = (UIImageView *)[view viewWithTag:101];
            UIImageView *imgViewBottom = (UIImageView *)[view viewWithTag:102];
            
            
            if(((NSNumber *)[_currentFlags objectAtIndex:Top]) == [NSNumber numberWithInteger:-1])
                _btnTops.enabled = NO;
            else
            {
                imgViewTop.image = [UIImage imageWithContentsOfFile:((PieceOfCloth *)[_tops objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Top]) intValue]]).picture];
                _btnTops.enabled = YES;
            }
            
            if(((NSNumber *)[_currentFlags objectAtIndex:Bottom]) == [NSNumber numberWithInteger:-1])
                _btnBottoms.enabled = NO;
            else
            {
                imgViewBottom.image = [UIImage imageWithContentsOfFile:((PieceOfCloth *)[_bottoms objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Bottom]) intValue]]).picture];
                _btnBottoms.enabled = YES;
            }
        }
        else
        {
            _btnDresses = (UIButton *)[view viewWithTag:3];
            UIImageView *imgViewDress = (UIImageView *)[view viewWithTag:103];
            
            if(((NSNumber *)[_currentFlags objectAtIndex:Ds]) == [NSNumber numberWithInteger:-1])
                _btnDresses.enabled = NO;
            else
            {
                imgViewDress.image = [UIImage imageWithContentsOfFile:((PieceOfCloth *)[_ds objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Ds]) intValue]]).picture];
                _btnDresses.enabled = YES;
            }
        }
        [self reload];
    }
    else
    {
        PieceOfCloth *p = [[self currentModalDataSource] objectAtIndex:index];
        ((UIImageView *)view).image = [UIImage imageWithContentsOfFile:p.picture];
    }
    
    return view;
}

-(void) reload
{
    [self addButonCheck];
    
    if((!_btnTops.isEnabled) && (!_btnBottoms.isEnabled) && (!_btnDresses.isEnabled) && (!_btnShoes.isEnabled) && (!_btnJackets.isEnabled) && (!_btnAccessories.isEnabled) && (!_btnJewleries.isEnabled))
    {
        [self.scrollView setContentOffset:
         CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
        KGDiscreetAlertView *alert = [[KGDiscreetAlertView alloc] init];
        alert.tapToHide = YES;
        [alert showWithText:@"You have no clothes for this occassion. To save an outfit you must have minimum top, bottom or dress in combination with shoes." inView:self.view maxWidth:320 delay:0];
    }
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if(carousel == _modalCarousel)
    {
        switch (option)
        {
            case iCarouselOptionWrap:
                return YES;
            case iCarouselOptionFadeMin:
                return -0.2;
            case iCarouselOptionFadeMax:
                return 0.2;
            case iCarouselOptionFadeRange:
                return 2.0;
            case iCarouselOptionSpacing:
                return value * 1.10f;
            case iCarouselOptionShowBackfaces:
                return NO;
            default:
                return value;
        }
    }
    else
            return value;
}

-(void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    if(carousel == _modalCarousel)
    {
    switch (_lastButton) {
        case 0:
            break;
        case 1:
            [_currentFlags replaceObjectAtIndex:Top withObject:[NSNumber numberWithInt:_modalCarousel.currentItemIndex]];
            break;
        case 2:
            [_currentFlags replaceObjectAtIndex:Bottom withObject:[NSNumber numberWithInt:_modalCarousel.currentItemIndex]];
            break;
        case 3:
            [_currentFlags replaceObjectAtIndex:Ds withObject:[NSNumber numberWithInt:_modalCarousel.currentItemIndex]];
            break;
        case 4:
            [_currentFlags replaceObjectAtIndex:Shoes withObject:[NSNumber numberWithInt:_modalCarousel.currentItemIndex]];
            break;
        case 5:
            [_currentFlags replaceObjectAtIndex:Jackets withObject:[NSNumber numberWithInt:_modalCarousel.currentItemIndex]];
            break;
        case 6:
            [_currentFlags replaceObjectAtIndex:Accessories withObject:[NSNumber numberWithInt:_modalCarousel.currentItemIndex]];
            break;
        case 7:
            [_currentFlags replaceObjectAtIndex:Jewlery withObject:[NSNumber numberWithInt:_modalCarousel.currentItemIndex]];
            break;
        }
        [self setButtonsAndImageViews];
        [_carousel reloadData];
    }
    else
        [self addButonCheck];
}

- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel
{
    return 1;
}

- (UIView *)carousel:(__unused iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 250.0f, 200.0f)];
        view.contentMode = UIViewContentModeScaleAspectFit;
    }

    ((UIImageView *)view).image = [UIImage imageNamed:@"placeholder_empty"];
    
    return view;
}

# pragma mark - IBAction Method

- (IBAction)buttonTap:(id)sender
{
    NSInteger index = ((UIButton*)sender).tag;
    _lastButton = index;
    
    UIImageView * bgimgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"han"]];
    UIView *modal = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    [modal setBackgroundColor: [UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1]];
    _modalCarousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, 40, 320, 280)];
    _modalCarousel.dataSource = self;
    _modalCarousel.delegate = self;
    [_modalCarousel setBackgroundColor:[UIColor whiteColor]];
    [modal addSubview:_modalCarousel];
    
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.frame = CGRectMake(145, 7, 30, 30);
    [deleteButton setImage:[UIImage imageNamed:@"trash"] forState:UIControlStateNormal];
    deleteButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [deleteButton addTarget:self action:@selector(deleteItem:)
          forControlEvents:UIControlEventTouchUpInside];
    
    if(_barButtonState == Save)
        deleteButton.enabled = NO;
    
    [modal addSubview:deleteButton];
    
    [self presentSemiView:modal withOptions:@{ KNSemiModalOptionKeys.backgroundView:bgimgv, KNSemiModalOptionKeys.pushParentBack : @(YES),
                                               KNSemiModalOptionKeys.parentAlpha : @(0.9) }];

    _modalCarousel.type = iCarouselTypeLinear;
    _modalCarousel.decelerationRate = 0.4;
    _modalCarousel.clipsToBounds = YES;
    _modalCarousel.pagingEnabled = YES;
    [_modalCarousel reloadData];
    switch (_lastButton) {
        case 1:
            [_modalCarousel scrollToItemAtIndex:[[_currentFlags objectAtIndex:Top] intValue] animated:NO];
            break;
        case 2:
            [_modalCarousel scrollToItemAtIndex:[[_currentFlags objectAtIndex:Bottom] intValue] animated:NO];
            break;
        case 3:
            [_modalCarousel scrollToItemAtIndex:[[_currentFlags objectAtIndex:Ds] intValue] animated:NO];
            break;
        case 4:
            [_modalCarousel scrollToItemAtIndex:[[_currentFlags objectAtIndex:Shoes] intValue] animated:NO];
            break;
        case 5:
            [_modalCarousel scrollToItemAtIndex:[[_currentFlags objectAtIndex:Jackets] intValue] animated:NO];
            break;
        case 6:
            [_modalCarousel scrollToItemAtIndex:[[_currentFlags objectAtIndex:Accessories] intValue] animated:NO];
            break;
        case 7:
            [_modalCarousel scrollToItemAtIndex:[[_currentFlags objectAtIndex:Jewlery] intValue] animated:NO];
            break;
    }
}

-(NSArray *)currentModalDataSource
{
    NSArray *result;
    switch (_lastButton) {
        case 0:
            result = [NSArray new];
            break;
        case 1:
            result = _tops;
            break;
        case 2:
            result = _bottoms;
            break;
        case 3:
            result = _ds;
            break;
        case 4:
            result = _shoes;
            break;
        case 5:
            result = _jackets;
            break;
        case 6:
            result = _accessories;
            break;
        case 7:
            result = _jewleries;
            break;
    }
    return result;
}


#pragma mark - IBAction Method for BarButton

- (IBAction)addOutfitButton:(id)sender
{
    if(_barButtonState == Add)
    {
        [_jacketsCheckbox setHidden:NO];
        [_accessoriesCheckbox setHidden:NO];
        [_jewleriesCheckbox setHidden:NO];
        
        _barButtonState = Save;
        _addOutfit.image = [UIImage imageNamed:@"save"];
        
    }
    else if(_barButtonState == Save)
    {
        NSMutableArray *tmp = [NSMutableArray array];
        if(_carousel.currentItemIndex == 0)
        {
            [tmp addObject:(PieceOfCloth *)[_tops objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Top]) intValue]]];
            [tmp addObject:(PieceOfCloth *)[_bottoms objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Bottom]) intValue]]];
        }
        else
            [tmp addObject:(PieceOfCloth *)[_ds objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Ds]) intValue]]];

        if(_btnJackets.isEnabled && _jacketsCheckbox.isChecked)
            [tmp addObject:(PieceOfCloth *)[_jackets objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Jackets]) intValue]]];
        
        if(_btnJewleries.isEnabled && _jewleriesCheckbox.isChecked)
            [tmp addObject:(PieceOfCloth *)[_jewleries objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Jewlery]) intValue]]];
        
        if(_btnAccessories.isEnabled && _accessoriesCheckbox.isChecked)
            [tmp addObject:(PieceOfCloth *)[_accessories objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Accessories]) intValue]]];
        
        [tmp addObject:(PieceOfCloth *)[_shoes objectAtIndex:[((NSNumber *)[_currentFlags objectAtIndex:Shoes]) intValue]]];
        
        _clothesForOutfitToSave = [NSArray arrayWithArray:tmp];

        
        _barButtonState = Add;
        _addOutfit.image = [UIImage imageNamed:@"pluska"];
        
        [_jacketsCheckbox setHidden:YES];
        [_accessoriesCheckbox setHidden:YES];
        [_jewleriesCheckbox setHidden:YES];
        
        [self performSegueWithIdentifier:@"SaveOutfitSegue" sender:self];
    }
}


# pragma mark - Navigation method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"SaveOutfitSegue"])
    {
        SaveOutfitViewController *save = [segue destinationViewController];
        save.clothes = _clothesForOutfitToSave;
    }
}


# pragma mark - IBAction method for Delete button from SemiModal

- (IBAction)deleteItem:(id)sender
{
    if(_modalCarousel.numberOfItems == 1)
        ((UIButton *)sender).enabled = NO;
    
    if (_modalCarousel.numberOfItems > 0)
    {
        NSInteger index = _modalCarousel.currentItemIndex;
        PieceOfCloth *p;
        switch (_lastButton) {
            case 0:
                break;
            case 1:
                p = [_tops objectAtIndex:index];
                [_tops removeObjectAtIndex:index];
                break;
            case 2:
                p = [_bottoms objectAtIndex:index];
                [_bottoms removeObjectAtIndex:index];
                break;
            case 3:
                p = [_ds objectAtIndex:index];
                [_ds removeObjectAtIndex:index];
                break;
            case 4:
                p = [_shoes objectAtIndex:index];
                [_shoes removeObjectAtIndex:index];
                break;
            case 5:
                p = [_jackets objectAtIndex:index];
                [_jackets removeObjectAtIndex:index];
                break;
            case 6:
                p = [_accessories objectAtIndex:index];
                [_accessories removeObjectAtIndex:index];
                break;
            case 7:
                p = [_jewleries objectAtIndex:index];
                [_jewleries removeObjectAtIndex:index];
                break;
        }
        BOOL success = [Data deletePieceOfCloth:p];
        
        if(success)
        {
            [_modalCarousel removeItemAtIndex:index animated:YES];
            [_modalCarousel scrollToItemAtIndex:_modalCarousel.currentItemIndex animated:YES];
            [self setButtonsAndImageViews];
            [_carousel reloadData];
        }
        else
        {
            self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Error"
                                                                  andMessage:@"Something went wrong. Try again."
                                                             andButtonTitles:@[@"OK"]
                                                               andTargetView:self.view];
            [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
            }];
        }
    }
}

@end
