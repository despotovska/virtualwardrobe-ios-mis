//
//  FourthViewController.h
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data.h"
#import "Outfit.h"
#import "OutfitDetailsViewController.h"
#import "AlertComponent.h"
#import <Social/Social.h>
#import "MGInstagram.h"
#import "KGDiscreetAlertView.h"

@interface FourthViewController : UIViewController <UIActionSheetDelegate>

@end
