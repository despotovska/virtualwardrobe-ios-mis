//
//  FirstViewController.m
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@property (strong, nonatomic) NSArray *occasions;

@end

@implementation FirstViewController

@synthesize occasions;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.topItem.title = @"Occasions";
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mala_hangers"]];
    
    occasions = [Data getOccasions];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


# pragma mark - UITableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self occasions] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	OccasionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OccasionCell"];
    if(cell == nil)
    {
        cell = [[OccasionCell alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:@"OccasionCell"];
    }
	cell.title.text = [[self occasions] objectAtIndex:[indexPath row]];
    cell.img1.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d%@",indexPath.row,@"0s.png"]];
    cell.img2.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d%@",indexPath.row,@"1s.png"]];
    cell.img1.alpha = 0.8;
    cell.img2.alpha = 0.8;
    cell.backgroundView.contentMode = UIViewContentModeBottom;
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aero"]];
    cell.backgroundView.alpha = 0.6;
    cell.backgroundColor =[UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


# pragma mark - Navigation Method

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowClothesForOccasion"])
    {
        WardrobeViewController *wardrobe = [segue destinationViewController];
        NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
        wardrobe.occasion = (myIndexPath.row + 1);
    }
}

@end
