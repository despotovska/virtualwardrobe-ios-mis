//
//  WardrobeViewController.h
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "BFPaperCheckbox.h"
#import "UIScrollView+EmptyDataSet.h"
#import "MBProgressHUD.h"
#import "KGDiscreetAlertView.h"
#import "UIColor+BFPaperColors.h"
#import "UIViewController+KNSemiModal.h"
#import "AppDelegate.h"
#import "SaveOutfitViewController.h"
#import "AlertComponent.h"

typedef enum PiecesOfClothTypes{
    Top,
    Bottom,
    Ds,
    Shoes,
    Jackets,
    Accessories,
    Jewlery
} PiecesOptions;

typedef enum ButtonStateTypes{
    Add,
    Save
} ButtonOptions;

@interface WardrobeViewController : UIViewController <iCarouselDataSource, iCarouselDelegate, BFPaperCheckboxDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property NSInteger occasion;

@end
