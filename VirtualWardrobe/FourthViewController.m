//
//  FourthViewController.m
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "FourthViewController.h"

@interface FourthViewController ()

@property (strong, nonatomic) Outfit *random;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (assign, nonatomic) BOOL isLiked;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *infoButton;
@property (strong, nonatomic) IBOutlet UIButton *shareButton;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraint;
@property (strong, nonatomic) AlertComponent *alertComponent;
@property (assign, nonatomic) int previous;
@property (strong, nonatomic) NSString *shareText;
@property (assign, nonatomic) int minutesSaved;
@property (strong, nonatomic) NSString *previousDate;

@end

@implementation FourthViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.constraint.constant = 485;
    _minutesSaved = 5;

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mala_hangers"]];

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.topItem.title = @" ";
    
    _shareText = @"The 'I don't have what to wear' days will stay in the past! #VirtualWardrobe";
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    _previous = 0;
    _isLiked = NO;
    _random = [Data getRandomOutfit];
    [_likeButton setBackgroundColor:[UIColor clearColor]];
    
    if(!_random)
    {
            _imgView.image = [UIImage imageNamed:@"random_empty"];
            _likeButton.enabled = _infoButton.enabled = _shareButton.enabled = NO;
            [_likeButton setImage:[UIImage imageNamed:@"heart_gray"] forState:UIControlStateNormal];
            _likeButton.alpha = 0.8;
            [self.scrollView setContentOffset:
             CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
            KGDiscreetAlertView *alert = [[KGDiscreetAlertView alloc] init];
            alert.tapToHide = YES;
            [alert showWithText:@"You have no outfits for this season." inView:self.view maxWidth:320 delay:3];
            [_imgView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
            [_imgView.layer setBorderWidth:1];
    }
    else
    {
        if((_previous == 0) || (_previous != 0 && _previous != [_random.oid integerValue]))
        {
            _imgView.image = [UIImage imageWithContentsOfFile:_random.picture];
            _likeButton.enabled = _infoButton.enabled = _shareButton.enabled = YES;
            [_likeButton setImage:[UIImage imageNamed:@"heart"] forState:UIControlStateNormal];
            _likeButton.alpha = 1;
            _previous = [_random.oid integerValue];
        }
        else
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSNumber *noOutfits = [prefs objectForKey:@"noOutfits"];
            NSString *msg;
            if([noOutfits intValue] == 0)
                msg = @"Add outfits to get suggestions.";
            
            if(_previous != 0)
                msg = @"Add more outfits to get more suggestions.";
            
            
            [self.scrollView setContentOffset:
                 CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
            KGDiscreetAlertView *alert = [[KGDiscreetAlertView alloc] init];
            alert.tapToHide = YES;
            [alert showWithText:msg inView:self.view maxWidth:320 delay:3];
        }
    }
    _imgView.contentMode = UIViewContentModeScaleAspectFit;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSNumber *time = [prefs objectForKey:@"time"];
    if([time intValue] == 0 && _random)
    {
        [self.scrollView setContentOffset:
         CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
        KGDiscreetAlertView *alert = [[KGDiscreetAlertView alloc] init];
        alert.tapToHide = YES;
        [alert showWithText:@"If you liked the suggestion and it saved you time, heart it." inView:self.view maxWidth:320 delay:3];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - Shake Event

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake )
    {
        _random = [Data getRandomOutfit];
        _isLiked = NO;
        [_likeButton setImage:[UIImage imageNamed:@"heart"] forState:UIControlStateNormal];
        
        if(!_random)
        {
            _imgView.image = [UIImage imageNamed:@"random_empty"];
            [_likeButton setImage:[UIImage imageNamed:@"heart_gray"] forState:UIControlStateNormal];
                    _likeButton.enabled = _infoButton.enabled = _shareButton.enabled = NO;
            _likeButton.alpha = 0.8;
        }
        else
        {
            if((_previous==0) || (_previous!=0 && _previous != [_random.oid integerValue]))
            {
                _imgView.image = [UIImage imageWithContentsOfFile:_random.picture];
                [_likeButton setImage:[UIImage imageNamed:@"heart"] forState:UIControlStateNormal];
                _likeButton.enabled = _infoButton.enabled = _shareButton.enabled = YES;
                _likeButton.alpha = 1;
                _previous = [_random.oid integerValue];
            }
            else
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                NSNumber *noOutfits = [prefs objectForKey:@"noOutfits"];
                NSString *msg;
                if([noOutfits intValue] == 0)
                    msg = @"Add outfits to get suggestions.";
                
                if(_previous != 0)
                    msg = @"Add more outfits to get more suggestions.";
                
                [self.scrollView setContentOffset:
                 CGPointMake(0, -self.scrollView.contentInset.top) animated:YES];
                KGDiscreetAlertView *alert = [[KGDiscreetAlertView alloc] init];
                alert.tapToHide = YES;
                [alert showWithText:msg inView:self.view maxWidth:320 delay:3];

            }
        }
    }
}


#pragma mark - IBAction Methods

- (IBAction)likeButtonTap:(id)sender
{
    if(!_isLiked)
    {
        NSLog(@"not");
        _previousDate = _random.lastWeared;
        NSDate *myDate = [NSDate date];
        NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
        [dateFormater setDateFormat:@"dd-MM-yyyy"];
        _random.lastWeared = [dateFormater stringFromDate:myDate];
        [Data updateLastForOutfit:_random];
        
        _isLiked = YES;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSNumber *time= [prefs objectForKey:@"time"];
        int value = [time intValue];
        time = [NSNumber numberWithInt:value + _minutesSaved];
        [prefs setObject:time forKey:@"time"];
        [((UIButton *)sender) setImage:[UIImage imageNamed:@"fullheart"] forState:UIControlStateNormal];
    }
    else
    {
         NSLog(@"yes");
        _random.lastWeared = _previousDate;
        [Data updateLastForOutfit:_random];
        _isLiked = NO;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSNumber *time= [prefs objectForKey:@"time"];
        int value = [time intValue];
        time = [NSNumber numberWithInt:value - _minutesSaved];
        [prefs setObject:time forKey:@"time"];
        [((UIButton *)sender) setImage:[UIImage imageNamed:@"heart"] forState:UIControlStateNormal];
    }
}

- (IBAction)shareButtonTap:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"  Twitter", @"  Instagram",  @"  Facebook", nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
        [self shareToTwitter];
    else if (buttonIndex == 1)
        [self shareToInstagram];
    else if (buttonIndex == 2)
        [self shareToFacebook];
}

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    [actionSheet.subviews enumerateObjectsUsingBlock:^(UIView *subview, NSUInteger idx, BOOL *stop) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)subview;
            [button.titleLabel setFont:[UIFont fontWithName:@"Wellfleet-Regular" size:18]];
            [button.titleLabel setTextAlignment:NSTextAlignmentRight];
            button.titleLabel.highlightedTextColor = [UIColor lightGrayColor];
            [button setTitleColor:[UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1] forState:UIControlStateNormal];
            if(idx<4)
                [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"social%d",idx-1]]
                        forState:UIControlStateNormal];
        }
    }];
}

- (void) shareToTwitter
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:_shareText];
        [tweetSheet addImage:_imgView.image];

        [tweetSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output;
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = @"Post Canceled";
                    break;
                case SLComposeViewControllerResultDone:
                    output = @"Post Sucessful";
                    break;
                default:
                    break;
            }
            
            self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Twitter Complition Message"
                                                                  andMessage:output
                                                             andButtonTitles:@[@"OK"]
                                                               andTargetView:self.view];
            [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
            }];
        }];
        [tweetSheet.tabBarController.tabBar setTintColor:[UIColor lightGrayColor]];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"You can't post right now"
                                                              andMessage:@"Make sure your device has an internet connection and you\n have at least one Twitter\n account setup"
                                                         andButtonTitles:@[@"OK"]
                                                           andTargetView:self.view];
        [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
        }];
    }
}

- (void) shareToInstagram
{
    if ([MGInstagram isAppInstalled])
    {
        UIImage *img = [self aspectFillImage:_imgView.image toSize:CGSizeMake(612, 612)];
        if([MGInstagram isImageCorrectSize:img])
        {
            [MGInstagram setPhotoFileName:kInstagramOnlyPhotoFileName];
            [MGInstagram postImage:img withCaption:_shareText inView:self.view];
        }
    }
    else
    {
        self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Instagram Not Installed"
                                                              andMessage:@"Instagram must be installed\n on the device in order\n to post images"
                                                         andButtonTitles:@[@"OK"]
                                                           andTargetView:self.view];
        [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
        }];
    }
}

- (void) shareToFacebook
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];

        [controller addImage:_imgView.image];
        
        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output;
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = @"Post Canceled";
                    break;
                case SLComposeViewControllerResultDone:
                    output = @"Post Sucessful";
                    break;
                default:
                    break;
            }
            
            self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Facebook Complition Message"
                                                                  andMessage:output
                                                             andButtonTitles:@[@"OK"]
                                                               andTargetView:self.view];
            [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
            }];
        }];
        
        [controller setInitialText:_shareText];
        [self presentViewController:controller animated:YES completion:Nil];
    }
    else {
        self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"You can't post right now"
                                                                andMessage:@"Make sure your device has an internet connection and you\n have at least one Facebook\n account setup"
                                                         andButtonTitles:@[@"OK"]
                                                           andTargetView:self.view];
        [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
        }];
    }
}


# pragma mark - Navigation Method

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"Details"])
    {
        OutfitDetailsViewController *detailView = [segue destinationViewController];
        detailView.outfit =_random;
        detailView.hidesBottomBarWhenPushed = YES;
    }
}

-(UIImage *)aspectFillImage:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    
    [[self scale:image toSize:CGSizeMake(495, 612)] drawInRect:CGRectMake(58,0,495,612)];
    UIImage *aspectScaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return aspectScaledImage;
}

- (UIImage *)scale:(UIImage *)image toSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

@end
