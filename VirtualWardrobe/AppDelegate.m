//
//  AppDelegate.m
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _d = [[Data alloc] init];
    [_d initialize];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1]];
    
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary  dictionaryWithObjectsAndKeys:
                                                           [UIColor whiteColor], NSForegroundColorAttributeName,
                                                           [UIFont fontWithName:@"Wellfleet-Regular" size:18.0], NSFontAttributeName, nil]];

    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:
     [NSDictionary  dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName,
      [UIFont fontWithName:@"Wellfleet-Regular" size:15.0], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1]];
    
    [[UILabel appearanceWhenContainedIn:[UITableViewHeaderFooterView class],nil] setFont:[UIFont fontWithName:@"Wellfleet-Regular" size:12.0]];
   
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if([prefs objectForKey:@"north"] == nil)
    {
        [prefs setBool:YES forKey:@"north"];
    }
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
