//
//  Data.m
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "Data.h"

@implementation Data

- (void)initialize
{
    [Data loadSqliteFileIntoDocumentsDirectoryIfNeeded];
    [Data createFolder];
    [Data createTables];
    
    _tops = [NSMutableArray array];
    _bottoms = [NSMutableArray array];
    _jackets = [NSMutableArray array];
    _ds = [NSMutableArray array];
    _shoes = [NSMutableArray array];
    _accessories = [NSMutableArray array];
    _jewleries = [NSMutableArray array];
}

+ (void)createFolder
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/VirtualWardrobe"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    if (![fileManager fileExistsAtPath:dataPath])
        [fileManager createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error ];
}


# pragma marrk - FMDB methods

+ (NSString *)getDBPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:@"Wardrobe.sqlite"];
    return path;
}

+ (void)loadSqliteFileIntoDocumentsDirectoryIfNeeded
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSString *strPath = [Data getDBPath];
    BOOL success = [fileManager fileExistsAtPath:strPath];
    
    if(!success)
    {
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Wardrobe.sqlite"];
        [fileManager copyItemAtPath:defaultDBPath toPath:strPath error:&error];
        if (error)
            NSLog(@"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        else
            NSLog(@"DB successfully created");
    }
    else
        NSLog(@"DB already existed");
}

+ (void)createTables
{
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];
    [database executeQuery:@"PRAGMA foreign_keys=ON"];
    
    [database executeUpdate:@"create table outfit(oid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, description	TEXT, occasion INTEGER NOT NULL, season INTEGER NOT NULL, picture TEXT INTEGER NOT NULL, last TEXT DEFAULT 'never')"];
    
    [database executeUpdate:@"create table pieceOfCloth(pid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, description TEXT, occasion INTEGER NOT NULL, type INTEGER NOT NULL, picture INTEGER NOT NULL)"];
    
    [database executeUpdate:@"create table o_poc(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, pid INTEGER NOT NULL, oid NOT NULL, FOREIGN KEY(pid) REFERENCES PieceOfCloth(pid), FOREIGN KEY(oid) REFERENCES Outfit(oid) ON DELETE CASCADE)"];
    
    [database close];
}

+ (int) addOutfit:(Outfit *)o forPieces:(NSArray *)pieces
{
    int returnValue = 0; //greska
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];
    [database executeQuery:@"PRAGMA foreign_keys=ON"];

    NSString *format = @"select o.* from outfit as o inner join o_poc as op on op.oid = o.oid where op.pid = %d";
    NSString *mainFotmat = @"%@ intersect %@";
    
    NSString *tmp = [NSString stringWithFormat:format,[((PieceOfCloth *)pieces[0]).pid intValue]];
    
    for (int i = 1; i< [pieces count]; i++) {
        tmp = [NSString stringWithFormat:mainFotmat,tmp,[NSString stringWithFormat:format,[((PieceOfCloth *)pieces[i]).pid intValue]]];
    }

    
    BOOL mainSuccess = YES;
    FMResultSet *result = [database executeQuery:tmp];
    while ([result next] && mainSuccess) {
        int i = [result intForColumn:@"oid"];
        FMResultSet *pom = [database executeQueryWithFormat:@"select count(pid) as vk from o_poc where oid = %d",i];
        
        while ([pom next])
        {
            int c = [pom intForColumn:@"vk"];
            if(c == [pieces count])
            {
                mainSuccess = NO;
                break;
            }
        }
    }
    
    BOOL success;
    if(mainSuccess)
    {
        success = [database executeUpdate:@"insert into outfit(description, occasion, season, picture) values(?,?,?,?)", o.description, [NSNumber numberWithInt:[o.occasion intValue]], [NSNumber numberWithInt:[o.season intValue]], o.picture];
    
        NSNumber *lastId = [NSNumber numberWithInt:(int)[database lastInsertRowId]];
    
        for (PieceOfCloth *item in pieces)
        {
            if(![database executeUpdate:@"insert into o_poc(pid, oid) values(?,?)", [NSNumber numberWithInt:[item.pid intValue]], lastId])
            {
                success = NO;
                break;
            }
        }
        
        if(success)
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSNumber *noOutfits= [prefs objectForKey:@"noOutfits"];
            int value = [noOutfits intValue];
            noOutfits = [NSNumber numberWithInt:value + 1];
            [prefs setObject:noOutfits forKey:@"noOutfits"];
            returnValue = 1;//ok
        }
        else
        {
            [database executeUpdate:@"delete from outfit where oid = ?",lastId];
            [database executeUpdate:@"delete from o_poc where oid = ?",lastId];
        }
    }
    else
        returnValue = 2; //duplikat

    [database close];

    return returnValue;
}

+ (BOOL) addPieceOfCloth:(PieceOfCloth *)p
{
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];
    
    BOOL success = [database executeUpdate:@"insert into pieceOfCloth(description, occasion, type, picture) values(?,?,?,?)", p.description,[NSNumber numberWithInt:[p.occasion intValue]], [NSNumber numberWithInt:[p.type integerValue]], p.picture];
    
    [database close];
    
    if(success)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSNumber *noPieces= [prefs objectForKey:@"noPieces"];
        int value = [noPieces intValue];
        noPieces = [NSNumber numberWithInt:value + 1];
        [prefs setObject:noPieces forKey:@"noPieces"];
    }
    
    return success;
}

- (void)getClothesForOccasion:(NSInteger)occasion
{
    [_tops removeAllObjects];
    [_bottoms removeAllObjects];
    [_ds removeAllObjects];
    [_shoes removeAllObjects];
    [_jackets removeAllObjects];
    [_jewleries removeAllObjects];
    [_accessories removeAllObjects];
    
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];

    FMResultSet *results = [database executeQueryWithFormat:@"select * from pieceOfCloth where occasion = %d", occasion];

    while([results next]) {
        PieceOfCloth *p = [[PieceOfCloth alloc] init];
        p.pid = [NSString stringWithFormat:@"%d",[results intForColumn:@"pid"]];
        p.description = [results stringForColumn:@"description"];
        p.picture = [results stringForColumn:@"picture"];
        NSInteger type = [results intForColumn:@"type"];
        p.type = [NSString stringWithFormat:@"%d",type];
        p.occasion = [NSString stringWithFormat:@"%d",occasion];
        switch (type) {
            case 1:
                [_tops addObject:p];
                break;
            case 2:
                [_bottoms addObject:p];
                break;
            case 3:
                [_ds addObject:p];
                break;
            case 4:
                [_shoes addObject:p];
                break;
            case 5:
                [_jackets addObject:p];
                break;
            case 6:
                [_accessories addObject:p];
                break;
            case 7:
                [_jewleries addObject:p];
                 break;
            default:
                break;
        }
    }
    
    [database close];
}

+ (NSMutableArray *)getOutfitsForSeason:(NSInteger)season
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];
    
    FMResultSet *results;
    results = [database executeQueryWithFormat:@"select * from outfit where season = %d", season];
    
    while([results next]) {
        Outfit *o = [[Outfit alloc] init];
        o.oid = [NSString stringWithFormat:@"%d",[results intForColumn:@"oid"]];
        o.description = [results stringForColumn:@"description"];
        o.picture = [results stringForColumn:@"picture"];
        o.occasion = [NSString stringWithFormat:@"%d",[results intForColumn:@"occasion"]];
        o.season = [NSString stringWithFormat:@"%d",season];
        o.lastWeared = [results stringForColumn:@"last"];
        
        [result addObject:o];
    }
    return result;
}

+ (NSMutableArray *)getAllOutfits
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];
    
    FMResultSet *results;
    results = [database executeQuery:@"select * from outfit"];
    
    while([results next]) {
        Outfit *o = [[Outfit alloc] init];
        o.oid = [NSString stringWithFormat:@"%d",[results intForColumn:@"oid"]];
        o.description = [results stringForColumn:@"description"];
        o.picture = [results stringForColumn:@"picture"];
        o.occasion = [NSString stringWithFormat:@"%d",[results intForColumn:@"occasion"]];
        o.season = [NSString stringWithFormat:@"%d",[results intForColumn:@"season"]];
        o.lastWeared = [results stringForColumn:@"last"];
        [result addObject:o];
    }
    return result;
}

+ (BOOL) updateOutfit:(Outfit *)o
{
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];
    [database executeQuery:@"PRAGMA foreign_keys=ON"];
    
    BOOL success = [database executeUpdateWithFormat:@"update outfit set description=%@ where oid = %d",o.description,[o.oid intValue]];
    
    [database close];
    
    return success;
}

+ (BOOL) updateLastForOutfit:(Outfit *)o
{
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];
    [database executeQuery:@"PRAGMA foreign_keys=ON"];
    
    BOOL success = [database executeUpdateWithFormat:@"update outfit set last=%@ where oid = %d",o.lastWeared,[o.oid intValue]];
    
    [database close];
    
    return success;
}

+ (BOOL) deleteOutfit:(Outfit *)o
{
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];
    [database executeQuery:@"PRAGMA foreign_keys=ON"];
    
    BOOL success = [database executeUpdateWithFormat:@"delete from outfit where oid = %d", [o.oid intValue]];

    [database close];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:o.picture] && success)
    {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath: o.picture error: &error];
        if(error != nil)
            NSLog(@"Failed to delete file with message '%@'.", [error localizedDescription]);
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSNumber *noOutfits= [prefs objectForKey:@"noOutfits"];
        int value = [noOutfits intValue];
        noOutfits = [NSNumber numberWithInt:value - 1];
        [prefs setObject:noOutfits forKey:@"noOutfits"];
    }
    
    return success;
}

+ (BOOL) deletePieceOfCloth:(PieceOfCloth *)p
{
    FMDatabase *database = [FMDatabase databaseWithPath:[Data getDBPath]];
    [database open];
    [database executeQuery:@"PRAGMA foreign_keys=ON"];
    
    FMResultSet *poc;
    poc = [database executeQueryWithFormat:@"select * from o_poc where pid = %d", [p.pid intValue]];
    
    int i = 0;
    
    while([poc next])
    {
        NSString *oid = [NSString stringWithFormat:@"%d",[poc intForColumn:@"oid"]];
        NSString *picture = [[database executeQueryWithFormat:@"select * from outfit where oid = %d",[oid intValue]] stringForColumn:@"picture"];
        
        [database executeUpdateWithFormat:@"delete from outfit where oid = %d", [oid intValue]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:picture])
        {
            NSError *error;
            [[NSFileManager defaultManager] removeItemAtPath: picture error: &error];
            if(error != nil)
                NSLog(@"Failed to delete file with message '%@'.", [error localizedDescription]);
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSNumber *noOutfits= [prefs objectForKey:@"noOutfits"];
            int value = [noOutfits intValue];
            noOutfits = [NSNumber numberWithInt:value - 1];
            [prefs setObject:noOutfits forKey:@"noOutfits"];
        }
        
        i++;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSNumber *noOutfits= [prefs objectForKey:@"noOutfits"];
    int value = [noOutfits intValue];
    noOutfits = [NSNumber numberWithInt:value - i];
    [prefs setObject:noOutfits forKey:@"noOutfits"];
    
    BOOL success = [database executeUpdateWithFormat:@"delete from pieceOfCloth where pid = %d", [p.pid intValue]];

    [database close];

    if ([[NSFileManager defaultManager] fileExistsAtPath:p.picture])
    {
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath: p.picture error: &error];
        if(error)
            NSLog(@"Failed to delete file with message '%@'.", [error localizedDescription]);
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSNumber *noPieces= [prefs objectForKey:@"noPieces"];
        int value = [noPieces intValue];
        noPieces = [NSNumber numberWithInt:value - 1];
        [prefs setObject:noPieces forKey:@"noPieces"];
    }
    
    return success;
}


# pragma mark - Data from plist

+ (Outfit *)getRandomOutfit
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    BOOL north = [prefs boolForKey:@"north"];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *date = [NSDate date];
    NSDateComponents *dateComponents = [gregorian components:(NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSInteger month = [dateComponents month];
    NSInteger day = [dateComponents day];
    NSInteger season = 0;
    
    if(month == 1 || month == 2)
        season = 4;
    else if(month == 4 || month == 5)
        season = 1;
    else if(month == 7 || month == 8)
        season = 2;
    else if(month == 10 || month == 11)
        season = 3;
    else if(month == 3)
    {
        if(day < 21)
            season = 4;
        else
            season = 1;
    }
    else if (month == 6)
    {
        if(day < 21)
            season = 1;
        else
            season = 2;
    }
    else if(month == 9)
    {
        if(day < 21)
            season = 2;
        else
            season = 3;
    }
    else
    {
        if(day < 21)
            season = 3;
        else
            season = 4;
    }
    
    if(!north)
    {
        if(season > 2)
            season -= 2;
        else
            season += 2;
    }
        
    NSMutableArray *outfits = [self getOutfitsForSeason:season];
    int temp = [outfits count];
    
    if(temp != 0)
        return [outfits objectAtIndex:(arc4random() % temp)];
    else
        return nil;
}

+ (NSString *)getRandomQuote
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"VWardrobe" ofType:@"plist"];
    NSDictionary *arraysPaths = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    NSArray *quotes = arraysPaths[@"Quotes"];
    int temp = [quotes count];
    NSString *q = [quotes objectAtIndex:(arc4random() % temp)];
    return q;
}

+ (NSArray *)getOccasions
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"VWardrobe" ofType:@"plist"];
    NSDictionary *arraysPaths = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    return arraysPaths[@"Occasions"];
}

+ (NSArray *)getTypes
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"VWardrobe" ofType:@"plist"];
    NSDictionary *arraysPaths = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    return arraysPaths[@"Types"];
}

+ (NSArray *)getSeasons
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"VWardrobe" ofType:@"plist"];
    NSDictionary *arraysPaths = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    return arraysPaths[@"Seasons"];
}

@end