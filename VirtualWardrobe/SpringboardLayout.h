#import <UIKit/UIKit.h>
#import "SpringboardLayoutAttributes.h"

@protocol SpringboardLayoutDelegate <UICollectionViewDelegateFlowLayout>

@required

- (BOOL) isDeletionModeActiveForCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout;

@end


@interface SpringboardLayout : UICollectionViewFlowLayout

@end
