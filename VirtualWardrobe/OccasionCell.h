//
//  OccasionCellTableViewCell.h
//  VirtualWardrobe
//
//  Created by alex on 9/4/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OccasionCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UILabel *title;

@end
