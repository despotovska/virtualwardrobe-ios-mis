//
//  ThirdViewController.m
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "ThirdViewController.h"

@interface ThirdViewController ()

@property (nonatomic,strong) NSMutableArray *outfits;
@property (nonatomic,strong) NSMutableArray *searchOutfits;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic) BOOL searching;
@property (nonatomic, strong) AlertComponent *alertComponent;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (nonatomic) BOOL isDeletionModeActive;

@end

@implementation ThirdViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
     
     self.collectionView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"mala_hangers"]];
     self.navigationController.navigationBar.topItem.title = @"";
     
     _searching = NO;
     _searchOutfits = [NSMutableArray new];
     
    _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    _searchBar.barTintColor = [UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1];
    _searchBar.tintColor = [UIColor whiteColor];
    _searchBar.placeholder = @"Search for outfits...";
     
     [self.collectionView setAllowsMultipleSelection:NO];
     UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(activateDeletionMode:)];
     longPress.delegate = self;
     [self.collectionView addGestureRecognizer:longPress];
     UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endDeletionMode:)];
     tap.delegate = self;
     [self.collectionView addGestureRecognizer:tap];
     
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     NSNumber *noOutfits = [prefs objectForKey:@"noOutfits"];
     if([noOutfits intValue] == 0)
     {
          KGDiscreetAlertView *alert = [[KGDiscreetAlertView alloc] init];
          alert.tapToHide = YES;
          [alert showWithText:@"To add an outfit go to the wardrobe tab, choose an occasion\n and save a combination you like." inView:self.view maxWidth:320 delay:5];
     }
}

- (void)viewWillAppear:(BOOL)animated
{
     [super viewWillAppear:animated];
     [self.navigationController setNavigationBarHidden:YES animated:animated];
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
          dispatch_async(dispatch_get_main_queue(), ^{
               _outfits =  [Data getAllOutfits];
               [MBProgressHUD hideHUDForView:self.view animated:YES];
               [_collectionView reloadData];
          });
     });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


# pragma mark - Getter

- (NSMutableArray *)searchResult
{
     if(_searching)
     {
          NSPredicate *predicate = [NSPredicate predicateWithFormat:@"description CONTAINS[c] %@", _searchBar.text];
          _searchOutfits = [NSMutableArray arrayWithArray:[_outfits filteredArrayUsingPredicate:predicate]];
          
          if(_searchOutfits.count != 0)
               return _searchOutfits;
          else
          {
               KGDiscreetAlertView *alert = [[KGDiscreetAlertView alloc] init];
               alert.tapToHide = YES;
               
               [alert showWithText:@"No results found for your search." inView:self.view maxWidth:320 delay:3];
          }
     }
     return _outfits;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No outfits loaded";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Wellfleet-Regular" size:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"\n\n\n\n\n\n";
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    return nil;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"placeholder_empty"];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    [scrollView becomeFirstResponder];
}


#pragma mark - UICollectionView DataSource Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
     return [self searchResult].count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
     return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
     Outfit *o = [self searchResult][indexPath.row];
     
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"OutfitCell" forIndexPath:indexPath];

     UIImageView *imgView = (UIImageView *)[cell viewWithTag:7];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    imgView.clipsToBounds = YES;
    imgView.image = [UIImage imageNamed:@"placeholder"];
    imgView.image = [UIImage imageWithContentsOfFile:o.picture];
     [imgView.layer setBorderWidth:1.0];
     [imgView.layer setCornerRadius:7.0];
     [imgView.layer setBorderColor:[UIColor grayColor].CGColor];
     
     [((OutfitCell *)cell).deleteButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
     [((OutfitCell *)cell).deleteButton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
     
    return cell;
}


#pragma mark - UISearchBar Delegate Methods

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
     [searchBar setShowsCancelButton:YES animated:YES];
     _searching = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
     [searchBar setText:@""];
     [searchBar setShowsCancelButton:NO animated:YES];
     [searchBar resignFirstResponder];
     _searching = NO;
     [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
     [searchBar resignFirstResponder];
     [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
     _searching = NO;
}


#pragma mark - Private Method

- (void)delete:(UIButton *)sender
{
     NSIndexPath *indexPath = [self.collectionView indexPathForCell:(UICollectionViewCell *)sender.superview.superview];
     BOOL success = [Data deleteOutfit:[[self searchResult] objectAtIndex:indexPath.row]];
     if(success)
     {
          [[self searchResult] removeObjectAtIndex:indexPath.row];
          [self.collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
     }
     else
     {
          self.alertComponent = [[AlertComponent alloc] initAlertWithTitle:@"Error"
                                                                andMessage:@"Something went wrong. Try again."
                                                           andButtonTitles:@[@"OK"]
                                                             andTargetView:self.view];
          [self.alertComponent showAlertViewWithSelectionHandler:^(NSInteger buttonIndex, NSString *buttonTitle) {
          }];
     }
}


#pragma mark - UICollectionView Delegate Methods

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
     if (_isDeletionModeActive) return NO;
     else return YES;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
     return UIEdgeInsetsMake(15, 15, 50, 15);
}


#pragma mark - Gesture-recognition Action Methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
     CGPoint touchPoint = [touch locationInView:self.collectionView];
     NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:touchPoint];
     if (indexPath && [gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]])
     {
          return NO;
     }
     return YES;
}

- (void)activateDeletionMode:(UILongPressGestureRecognizer *)gr
{
     if (gr.state == UIGestureRecognizerStateBegan)
     {
          NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[gr locationInView:self.collectionView]];
          if (indexPath)
          {
               _isDeletionModeActive = YES;
               SpringboardLayout *layout = (SpringboardLayout *)self.collectionView.collectionViewLayout;
               [layout invalidateLayout];
               [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
          }
     }
}

- (void)endDeletionMode:(UITapGestureRecognizer *)gr
{
     if (_isDeletionModeActive)
     {
          NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:[gr locationInView:self.collectionView]];
          if (!indexPath)
          {
               _isDeletionModeActive = NO;
               SpringboardLayout *layout = (SpringboardLayout *)self.collectionView.collectionViewLayout;
               [layout invalidateLayout];
               [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
          }
     }
}


#pragma mark - SpringBoard Layout Delegate

- (BOOL) isDeletionModeActiveForCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout
{
     return _isDeletionModeActive;
}


# pragma mark - Navigation Method

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
     if ([[segue identifier] isEqualToString:@"OutfitDetails"])
     {
          OutfitDetailsViewController *detailView = [segue destinationViewController];
          NSIndexPath *myIndexPath = [[self.collectionView indexPathsForSelectedItems] objectAtIndex:0];
          detailView.outfit = _outfits[myIndexPath.row];
          detailView.hidesBottomBarWhenPushed = YES;
     }
}

@end
