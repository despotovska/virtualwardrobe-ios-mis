//
//  AppDelegate.h
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Data.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) Data *d;
@property (strong,nonatomic) UIImage *selectedImage;

@end
