//
//  Outfit.h
//  VirtualWardrobe
//
//  Created by alex on 8/24/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Outfit : NSObject

@property (nonatomic, strong) NSString *oid;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *occasion;
@property (nonatomic, strong) NSString *season;
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *lastWeared;

@end
