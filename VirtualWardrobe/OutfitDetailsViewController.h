//
//  OutfitDetailsViewController.h
//  VirtualWardrobe
//
//  Created by alex on 9/5/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Outfit.h"
#import "Data.h"
#import "UIViewController+KNSemiModal.h"
#import "AlertComponent.h"

@interface OutfitDetailsViewController : UIViewController <UITextViewDelegate>

@property (strong,nonatomic) Outfit *outfit;

@end
