//
//  TabBarController.h
//  VirtualWardrobe
//
//  Created by alex on 8/22/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuComponent.h"

@interface TabBarController : UITabBarController

@end
