//
//  OutfitCell.h
//  VirtualWardrobe
//
//  Created by alex on 8/27/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SpringboardLayoutAttributes.h"

@interface OutfitCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

@end
