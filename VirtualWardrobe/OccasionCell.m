//
//  OccasionCellTableViewCell.m
//  VirtualWardrobe
//
//  Created by alex on 9/4/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import "OccasionCell.h"

@implementation OccasionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)awakeFromNib
{
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setFrame:(CGRect)frame
{
    frame.origin.x += 5;
    frame.size.width -= 2 * 5;
    frame.origin.y += 5;
    frame.size.height -= 2*10;
    [super setFrame:frame];
}

@end
