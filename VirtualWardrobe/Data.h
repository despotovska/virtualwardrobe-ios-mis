//
//  Data.h
//  VirtualWardrobe
//
//  Created by alex on 8/1/14.
//  Copyright (c) 2014 alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Outfit.h"
#import "PieceOfCloth.h"
#import "FMDatabase.h"

@interface Data : NSObject

@property (strong) NSMutableArray *tops;
@property (strong) NSMutableArray *bottoms;
@property (strong) NSMutableArray *jackets;
@property (strong) NSMutableArray *ds;
@property (strong) NSMutableArray *shoes;
@property (strong) NSMutableArray *accessories;
@property (strong) NSMutableArray *jewleries;

- (void)initialize;
+ (int)addOutfit:(Outfit *)o forPieces:(NSArray *)pieces;
+ (BOOL)addPieceOfCloth:(PieceOfCloth *)p;
- (void)getClothesForOccasion:(NSInteger)occasion;
+ (NSMutableArray *)getOutfitsForSeason:(NSInteger)season;
+ (Outfit *)getRandomOutfit;
+ (NSMutableArray *)getAllOutfits;
+ (BOOL)updateOutfit:(Outfit *)o;
+ (BOOL) updateLastForOutfit:(Outfit *)o;
+ (BOOL) deletePieceOfCloth:(PieceOfCloth *)p;
+ (BOOL) deleteOutfit:(Outfit *)o;
+ (NSString *)getRandomQuote;
+ (NSArray *)getOccasions;
+ (NSArray *)getTypes;
+ (NSArray *)getSeasons;

@end
