//
//  MyImagePickerViewController.h
//  cameratestapp
//
//  Created by pavan krishnamurthy on 6/24/14.
//  Copyright (c) 2014 pavan krishnamurthy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKImageCropViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "AppDelegate.h"

@protocol PKImagePickerViewControllerDelegate <NSObject>

-(void)imageSelected:(UIImage*)img;
-(void)imageSelectionCancelled;

@end

@interface PKImagePickerViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,GKImageCropControllerDelegate>

@property(nonatomic,strong) id<PKImagePickerViewControllerDelegate> delegate;
@property (assign,nonatomic) BOOL dress;

@end
