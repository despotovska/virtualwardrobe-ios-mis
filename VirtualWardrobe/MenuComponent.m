//
//  MenuComponent.m
//  DynamicsDemo
//
//  Created by Gabriel Theodoropoulos on 31/3/14.
//  Copyright (c) 2014 Gabriel Theodoropoulos. All rights reserved.
//

#import "MenuComponent.h"


@interface MenuComponent()

@property (nonatomic, strong) UIView *menuView;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIView *targetView;
@property (nonatomic, strong) UITableView *optionsTableView;
@property (nonatomic, strong) NSArray *menuOptions;
@property (nonatomic, strong) NSArray *menuOptionImages;
@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic) MenuDirectionOptions menuDirection;
@property (nonatomic) CGRect menuFrame;
@property (nonatomic) CGRect menuInitialFrame;
@property (nonatomic) BOOL isMenuShown;
@property (nonatomic, strong) void(^selectionHandler)(NSInteger);

-(void)setupMenuView;
-(void)setupBackgroundView;
-(void)setupOptionsTableView;
-(void)setInitialTableViewSettings;
-(void)setupSwipeGestureRecognizer;
-(void)hideMenuWithGesture:(UISwipeGestureRecognizer *)gesture;
-(void)toggleMenu;

@end


@implementation MenuComponent

-(id)initMenuWithFrame:(CGRect)frame targetView:(UIView *)targetView direction:(MenuDirectionOptions)direction options:(NSArray *)options optionImages:(NSArray *)optionImages
{

    if (self = [super init]) {
        self.menuFrame = frame;
        self.targetView = targetView;
        self.menuDirection = direction;
        self.menuOptions = [NSArray arrayWithArray:options];
        if (optionImages != nil) {
            self.menuOptionImages = [NSArray arrayWithArray:optionImages];
        }
        
        [self setupBackgroundView];
        [self setupMenuView];
        [self setupOptionsTableView];
        [self setInitialTableViewSettings];
        [self setupSwipeGestureRecognizer];
        self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.targetView];
        self.optionCellHeight = 50.0;
        self.acceleration = 15.0;
        self.isMenuShown = NO;
    }
        return self;
}


#pragma mark - Private method implementation
    
-(void)setupMenuView
{
    if (self.menuDirection == menuDirectionLeftToRight) {
        self.menuInitialFrame = CGRectMake(-self.menuFrame.size.width,
                                           self.menuFrame.origin.y,
                                           self.menuFrame.size.width,
                                           self.menuFrame.size.height);
    }
    else{
        self.menuInitialFrame = CGRectMake(self.targetView.frame.size.width,
                                           self.menuFrame.origin.y,
                                           self.menuFrame.size.width,
                                           self.menuFrame.size.height);
    }
    
    self.menuView = [[UIView alloc] initWithFrame:self.menuInitialFrame];
    [self.menuView setBackgroundColor:[UIColor colorWithRed:(25/255.0) green:(104/255.0) blue:(80/255.0) alpha:1.0]];
    [self.targetView addSubview:self.menuView];
}

-(void)setupBackgroundView
{
    self.backgroundView = [[UIView alloc] initWithFrame:self.targetView.frame];
    [self.backgroundView setBackgroundColor:[UIColor grayColor]];
    [self.backgroundView setAlpha:0.0];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [self.backgroundView addGestureRecognizer:tapRecognizer];
    [self.targetView addSubview:self.backgroundView];
}

-(void)setupOptionsTableView
{
    self.optionsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.menuFrame.size.width, self.menuFrame.size.height) style:UITableViewStylePlain];
    [self.optionsTableView setBackgroundColor:[UIColor clearColor]];
    [self.optionsTableView setScrollEnabled:NO];
    [self.optionsTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.menuView addSubview:self.optionsTableView];
    self.optionsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.menuFrame.size.width, 150)];
    [self.optionsTableView setDelegate:self];
    [self.optionsTableView setDataSource:self];
}

- (void)hide:(UITapGestureRecognizer*)sender
{
    [self toggleMenu];
    self.isMenuShown = NO;
}

-(void)setInitialTableViewSettings
{
    self.tableSettings = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                          [UIFont fontWithName:@"Wellfleet-Regular" size:22.0], @"titleFont",
                          [UIFont fontWithName:@"Wellfleet-Regular" size:20.0], @"timeFont",
                          [UIFont fontWithName:@"Wellfleet-Regular" size:18.0], @"font",
                          [UIFont fontWithName:@"Wellfleet-Regular" size:26.0], @"numFont",
                          [NSNumber numberWithInt:NSTextAlignmentCenter], @"textAlignment",
                          [UIColor whiteColor], @"textColor",
                          [NSNumber numberWithInt:UITableViewCellEditingStyleNone], @"selectionStyle",
                          nil];
}

-(void)setupSwipeGestureRecognizer
{
    UISwipeGestureRecognizer *hideMenuGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideMenuWithGesture:)];
    if (self.menuDirection == menuDirectionLeftToRight) {
        hideMenuGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    }
    else{
        hideMenuGesture.direction = UISwipeGestureRecognizerDirectionRight;
    }
    [self.menuView addGestureRecognizer:hideMenuGesture];
}

-(void)toggleMenu
{
    [self.animator removeAllBehaviors];
    CGFloat gravityDirectionX;
    CGPoint collisionPointFrom, collisionPointTo;
    CGFloat pushMagnitude = self.acceleration;
    if (!self.isMenuShown) {
        if (self.menuDirection == menuDirectionLeftToRight) {
            gravityDirectionX = 1.0;
            collisionPointFrom = CGPointMake(self.menuFrame.size.width, self.menuFrame.origin.y);
            collisionPointTo = CGPointMake(self.menuFrame.size.width, self.menuFrame.size.height);
        }
        else{
            gravityDirectionX = -1.0;
            collisionPointFrom = CGPointMake(self.targetView.frame.size.width - self.menuFrame.size.width, self.menuFrame.origin.y);
            collisionPointTo = CGPointMake(self.targetView.frame.size.width - self.menuFrame.size.width, self.menuFrame.size.height);
            pushMagnitude = (-1) * pushMagnitude;
        }
        [self.backgroundView setAlpha:0.3];
    }
    else{
        if (self.menuDirection == menuDirectionLeftToRight) {
            gravityDirectionX = -1.0;
            collisionPointFrom = CGPointMake(-self.menuFrame.size.width, self.menuFrame.origin.y);
            collisionPointTo = CGPointMake(-self.menuFrame.size.width, self.menuFrame.size.height);
            pushMagnitude = (-1) * pushMagnitude;
        }
        else{
            gravityDirectionX = 1.0;
            collisionPointFrom = CGPointMake(self.targetView.frame.size.width + self.menuFrame.size.width, self.menuFrame.origin.y);
            collisionPointTo = CGPointMake(self.targetView.frame.size.width + self.menuFrame.size.width, self.menuFrame.size.height);
        }
        [self.backgroundView setAlpha:0.0];
    }
    
    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.menuView]];
    [gravityBehavior setGravityDirection:CGVectorMake(gravityDirectionX, 0.0)];
    [self.animator addBehavior:gravityBehavior];
    
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[self.menuView]];
    [collisionBehavior addBoundaryWithIdentifier:@"collisionBoundary"
                                       fromPoint:collisionPointFrom
                                         toPoint:collisionPointTo];
    [self.animator addBehavior:collisionBehavior];
    
    UIDynamicItemBehavior *itemBehavior = [[UIDynamicItemBehavior alloc] initWithItems:@[self.menuView]];
    [itemBehavior setElasticity:0.35];
    [self.animator addBehavior:itemBehavior];
    
    
    UIPushBehavior *pushBehavior = [[UIPushBehavior alloc] initWithItems:@[self.menuView] mode:UIPushBehaviorModeInstantaneous];
    [pushBehavior setMagnitude:pushMagnitude];
    [self.animator addBehavior:pushBehavior];
}

-(void)hideMenuWithGesture:(UISwipeGestureRecognizer *)gesture
{
    [self toggleMenu];
    self.isMenuShown = NO;
}


#pragma mark - Public method implementation

-(void)showMenuWithSelectionHandler:(void (^)(NSInteger))handler
{
    if (!self.isMenuShown) {
        self.selectionHandler = handler;
        [self toggleMenu];
        self.isMenuShown = YES;
    }
}


#pragma mark - TableView Delegate and Datasource method implementation

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                  tableView.bounds.size.width, 50)];
    headerView.backgroundColor = [UIColor clearColor];
    
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(10, 7, 30, 30)];
    if (self.menuOptionImages != nil) {
        [img setImage:[UIImage imageNamed:[self.menuOptionImages objectAtIndex:0]]];
        [img setTintColor:[UIColor whiteColor]];
        [headerView addSubview:img];
    }

    UILabel *result = [[UILabel alloc] initWithFrame:CGRectMake(45, 0, tableView.bounds.size.width, 50)];
    [result setFont:[self.tableSettings objectForKey:@"titleFont"]];
    result.text = [self.menuOptions objectAtIndex:0];
    [result setTextColor:[self.tableSettings objectForKey:@"textColor"]];
    [headerView addSubview:result];

    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ([self.menuOptions count]-1)/2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionCell"];
    UILabel *myLabel;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"optionCell"];
        
        myLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 65, 100, 40)];
        myLabel.tag = 111;
        [myLabel setTextAlignment:[[self.tableSettings objectForKey:@"textAlignment"] intValue]];
        [myLabel setFont:[self.tableSettings objectForKey:@"numFont"]];
        [myLabel setTextColor:[self.tableSettings objectForKey:@"textColor"]];
        [cell.contentView addSubview:myLabel];
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell setSelectionStyle:[[self.tableSettings objectForKey:@"selectionStyle"] intValue]];
    
    if(indexPath.row == 0)
        [myLabel setFont:[self.tableSettings objectForKey:@"timeFont"]];
    
    myLabel = (UILabel*)[cell.contentView viewWithTag:111];
    myLabel.text = [self.menuOptions objectAtIndex:(indexPath.row+4)];

    cell.textLabel.text = [self.menuOptions objectAtIndex:(indexPath.row+1)];
    [cell.textLabel setFont:[self.tableSettings objectForKey:@"timeFont"]];
    [cell.textLabel setTextAlignment:[[self.tableSettings objectForKey:@"textAlignment"] intValue]];
    [cell.textLabel setTextColor:[self.tableSettings objectForKey:@"textColor"]];
    
    if (self.menuOptionImages != nil)
    {
        [cell.imageView setImage:[UIImage imageNamed:[self.menuOptionImages objectAtIndex:(indexPath.row+1)]]];
        [cell.imageView setTintColor:[UIColor whiteColor]];
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[tableView cellForRowAtIndexPath:indexPath] setSelected:NO];
    self.selectionHandler(indexPath.row);
}

@end
